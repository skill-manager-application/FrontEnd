import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JwtHelperService } from '@auth0/angular-jwt';
import { EntryService } from '../Services/AuthServices/entry.service';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor(public authService: TokenStorageService, public router: Router,private _snackBar: MatSnackBar,private jwtHelper : JwtHelperService,private entryService : EntryService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      const jwtToken = this.authService.getToken();
      const decodedToken: any = this.authService.getToken() != null ? jwt_decode(jwtToken as string) : null;
      const userRole = decodedToken != null ? decodedToken.role : null;
    if(!jwtToken || this.jwtHelper.isTokenExpired(jwtToken)) {
      // check Wither the real problem is the expiration of the
      if(this.jwtHelper.isTokenExpired(this.authService.getToken())){
        this._snackBar.open("Your session is Expired please try to loggIn again", '❌');
        this.entryService.signOut();
        this.router.navigate(['/SignIn'], { queryParams: { returnUrl: state.url } });
      }else{
        this._snackBar.open("Access Denied !!!", '❌');
        this.router.navigate(['/SignIn'], { queryParams: { returnUrl: state.url } });
      }
    }else{
      if(route.data['role'] && route.data['role'].indexOf(userRole) === -1){
        this._snackBar.open("Access Denied,Role Not Granted !!!", '❌');
        this.router.navigate(['/Dashboard'], { queryParams: { returnUrl: state.url } })
        return false;
      }
      else{
        return true;
      }
    }
    console.log(jwtToken)
    return true;
  }

}
