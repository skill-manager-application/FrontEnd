import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class SecureInnerPagesGuard implements CanActivate {
  constructor(public tokenStorageService: TokenStorageService, public router: Router,private _snackBar: MatSnackBar,private jwthelper : JwtHelperService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.tokenStorageService.getToken() && !this.jwthelper.isTokenExpired(this.tokenStorageService.getToken()) ) {
      this._snackBar.open("Access Denied, You are already LoggedIn !!!", '❌');
      this.router.navigate(['/Dashboard'], { queryParams: { returnUrl: state.url } })
    }
    return true;
    }

}
