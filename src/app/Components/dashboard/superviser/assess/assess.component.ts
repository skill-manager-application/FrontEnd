import { Component } from '@angular/core';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { UserInfo } from 'src/app/models/UserInfo';

@Component({
  selector: 'app-assess',
  templateUrl: './assess.component.html',
  styleUrls: ['./assess.component.css'],
})
export class AssessComponent {
  UserList: UserEntity[] = [];
  UserInfoList: any[] = [];
  constructor(
    private userInfoservice: UserInfoService,
    private matSnackBar: MatSnackBar,
    private tokenStorage: TokenStorageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.refreshList();
  }

  //Get All UserInfo Related to the current Authenticated User
  refreshList() {
    this.UserList = [];
    this.UserInfoList = [];
    const userAuth = this.tokenStorage.getUser();
    this.userInfoservice.getUsersBySuperViserId(userAuth).subscribe(
      (data :UserInfo[]) => {
        this.UserInfoList = data;
        this.getData();
      },
      () => {
        this.matSnackBar.open('Error while loading data', '❌');
      }
    );
  }

  // Get all Users that have superviser is the current authenticated user
  getData() {
    this.UserInfoList.forEach((el: any) => {
      el.assesed = true;
      this.userService.get(el.userId).subscribe((res: UserEntity) => {
        el.userName = res.lastname + ' ' + res.firstname;
      });
      for (let index = 0; index < el.skills.length; index++) {
        if (el.skills[index].supervisorAssignment == 0) {
          el.assesed = false;
          el.skills[index].selfAssignment != 0
            ? (el.selfAssignment = true)
            : (el.selfAssignment = false);
        }
        //supervised
        else {
          el.skills[index].selfAssignment != 0
            ? (el.selfAssignment = true)
            : (el.selfAssignment = false);
        }
      }
    });
  }
}
