import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { DepartmentService } from 'src/app/Services/Soprahr/department.service';
import { RoleService } from 'src/app/Services/Soprahr/role.service';
import { TeamService } from 'src/app/Services/Teams/team.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { Teams } from 'src/app/models/Teams';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, map } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';
import { DepartmentEntity } from 'src/app/models/SopraModels/DepartmentEntity';
import { Router } from '@angular/router';

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.css'],
})
export class AssignComponent {
  user!: UserEntity;
  option: boolean = false;

  skills = new FormControl('');
  roles = new FormControl('');
  teams = new FormControl('');
  departments = new FormControl('');
  myControl = new FormControl('');

  skillsList: any[] = [];
  rolesList: RoleEntity[] = [];
  teamsList: Teams[] = [];
  departmentsList: DepartmentEntity[] = [];
  usersList: string[] = [];

  usersListFilter: any[] = [];

  departmentsListUsers: string[] = [];
  rolesListUsers: string[] = [];
  teamsListUsers: string[] = [];

  ListUsersSearch: UserEntity[] = [];
  options: UserEntity[] = [];
  filteredOptions!: Observable<UserEntity[]>;
  constructor(
    private skillService: SkillService,
    private teamService: TeamService,
    private roleService: RoleService,
    private departmentService: DepartmentService,
    private userService: UserService,
    private userinfoService: UserInfoService,
    private matSnackBar: MatSnackBar,
    private router : Router
  ) {
    this.getSkills();
    this.getRoles();
    this.getTeams();
    this.getDepartments();
  }

  //Get Selected teams
  getUsersByTeam() {
    this.teamsListUsers = [];
    let list = this.teams.value as unknown as string[];
    if (Array.isArray(list)) {
      list.forEach((element: any) => {
        this.teamService.get(element).subscribe((res: any) => {
          this.teamsListUsers = this.usersList.concat(res.usersId);
        });
      });
    }
  }
  //Get Selected Department
  getUsersByDepartment() {
    this.departmentsListUsers = [];
    let list = this.departments.value as unknown as string[];
    if (Array.isArray(list)) {
      list.forEach((element: any) => {
        this.userService.getUsersByDepartmentIdUserResponse(element).subscribe(
          (res: any[]) => {
            res.forEach((el: any) => {
              this.departmentsListUsers.push(el.userId);
            });
          },
          () => {
            this.matSnackBar.open(
              'We Found An Error During Getting Users by department Please refresh this page  !!!',
              '❌'
            );
          }
        );
      });
    }
  }
  // Get Selected Skill
  getUsersBySkills() {
    if (this.skills.value != '') {
      this.userinfoService
        .getUsersBySkillId(this.skills.value as string)
        .subscribe(
          (res: any) => {
            //      this.skillsList = res.usersId;
            res.forEach((element: any) => {
              this.usersListFilter.push(element.userId);
            });
          },
          () => {
            this.matSnackBar.open(
              'Error while trying to get Departments please try to refresh page',
              '❌'
            );
          }
        );
    }
  }
  // Get Roles from DataBase
  getUsersByRole() {
    this.rolesListUsers = [];
    let list = this.roles.value as unknown as string[];
    if (Array.isArray(list)) {
      list.forEach((element: any) => {
        this.userService.getUsersByRoleId(element).subscribe(
          (res: UserEntity[]) => {
            res.forEach((el: UserEntity) => {
              this.rolesListUsers.push(el.userId);
            });
          },
          () => {
            this.matSnackBar.open(
              'Error while trying to get role/user please try to refresh page',
              '❌'
            );
          }
        );
      });
    }
  }
  // Get Skill from DataBase
  getSkills() {
    this.skillService.getAll().subscribe(
      (response: any) => {
        this.skillsList = response;
      },
      () => {
        this.matSnackBar.open(
          'Error while trying to get Skills please try to refresh page',
          '❌'
        );
      }
    );
  }
  // Get Roles from DataBase
  getRoles() {
    this.roleService.getAll().subscribe(
      (response: any) => {
        this.rolesList = response;
      },
      () => {
        this.matSnackBar.open(
          'Error while trying to get Roles please try to refresh page',
          '❌'
        );
      }
    );
  }
  // Get Departments from DataBase
  getDepartments() {
    this.departmentService.getAll().subscribe(
      (response: any) => {
        this.departmentsList = response;
      },
      () => {
        this.matSnackBar.open(
          'Error while trying to get Departments please try to refresh page',
          '❌'
        );
      }
    );
  }
  //Get Teams from DataBase
  getTeams() {
    this.teamService.getAll().subscribe(
      (response: any) => {
        this.teamsList = response;
      },
      () => {
        this.matSnackBar.open(
          'Error while trying to get Teams please try to refresh page',
          '❌'
        );
      }
    );
  }
  // Filter List From users that already have the selected skill
  removeUsersHasSkill(option: boolean = true) {
    this.getUsersByDepartment();
    this.getUsersByTeam();
    this.getUsersBySkills();
    this.getUsersByRole();
    setTimeout(() => {
      this.usersList = this.teamsListUsers;
      this.usersList = this.usersList.concat(
        this.departmentsListUsers,
        this.rolesListUsers
      );
      if (this.skills.value != '') {
        // Remove duplicates from usersList
        this.usersList = [...new Set(this.usersList)];

        // Filter out user IDs based on usersListFilter
        this.usersList = this.usersList.filter(
          (userId) => !this.usersListFilter.includes(userId)
        );
        if (option) {
          this.updateUsers();
        }
      } else {
        this.matSnackBar.open('Please select a skill', 'OK');
      }
    }, 150);
  }
  // Update UsersInfos in DataBase (Assinged Skills)
  updateUsers() {
    this.userinfoService
      .addSkillToListUsers(this.skills.value as string, this.usersList)
      .subscribe(
        (res: any) => {
          this.matSnackBar.open('Users have been added to the skill');
          setTimeout(() => {
            this.router
          .navigateByUrl('/', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/Dashboard/SuperViser/Assign']);
          });
          }, 500);
        },
        () => {
          this.matSnackBar.open(
            'Error while trying to get user skill infos please try to refresh page',
            '❌'
          );
        }
      );
  }

  // case single User

  ngOnInit() {
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map((value) => this._filter(value || ''))
    );
  }
  // Adding filter to the users
  private _filter(value: string): UserEntity[] {
    const filterValue = value.toLowerCase();
    return this.options.filter((user) =>
      user.email.toLowerCase().includes(filterValue)
    );
  }
  //Get Users on change the value of the input
  changes(event: MatAutocompleteSelectedEvent): void {
    const selectedUserId = event.option.value;
    const selectedUser = this.options.find(
      (user) => user.email === selectedUserId
    );
    this.user = this.options.find(
      (user) => user.email === selectedUserId
    ) as UserEntity;
    // Use the selected user as needed
  }
  //Get Users
  getUsersToAdd() {
    if (this.option == false) {
      this.option = true;
      this.userService.getusers().subscribe(
        (res: UserEntity[]) => {
          this.ListUsersSearch = res;
          this.options = res;
        },
        () => {
          this.matSnackBar.open(
            'We Found An Error During Getting Users Please refresh this page  !!!',
            '❌'
          );
        }
      );
    } else {
      this.option = false;
    }
  }
  // Update User's Skill Information By adding a new skill
  UpdateUser() {
    if (this.user != null) {
      this.userinfoService
        .addSkillToListUsers(this.skills.value as string, [this.user.userId])
        .subscribe(
          (res: any) => {
            this.matSnackBar.open('skill Added Successfully');
          },
          (error) => {
            this.matSnackBar.open(
              'We Found An Error During Adding skill to User Please refresh this page  !!!',
              '❌'
            );
          }
        );
    } else {
      this.matSnackBar.open('Please Select User', '❌');
    }
  }
}
