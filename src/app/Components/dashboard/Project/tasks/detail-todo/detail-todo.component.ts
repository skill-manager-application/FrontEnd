import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/Services/Teams/project.service';
import { TodosService } from 'src/app/Services/Teams/todos.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { TaskUpperRoles } from '../../../../../../assets/roles.constants';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { Task, checkList } from 'src/app/models/Task';

@Component({
  selector: 'app-detail-todo',
  templateUrl: './detail-todo.component.html',
  styleUrls: ['../../../../../../assets/Project.css'],
})
export class DetailTodoComponent {
  id!: any;
  todoData: Task = new Task();
  taskMembers: any[] = [];
  superViser!: UserEntity;
  check: boolean = false;
  newCheck: string = '';
  editTask: boolean = false;
  projectMembers: any[] = [];
  selectedUsers: string[] = [];
  currentDate = new Date();

  accessUser: boolean =
    TaskUpperRoles.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;

  constructor(
    public ar: ActivatedRoute,
    private todosService: TodosService,
    private userService: UserService,
    private projectService: ProjectService,
    private tokenStorage: TokenStorageService,
    private matSnackBar: MatSnackBar,
  ) {
    this.ar.params.subscribe((data) => {
      this.id = data['id'];
    });
    this.todosService.get(this.id).subscribe((res) => {
      this.todoData = res;
      this.todoData.endTime = this.todoData.endTime.substring(0, 10);
      this.getusers();
    });
  }
  //Make a Sub-Task as Done
  markSubTaskAsDone() {
    this.todoData.status = 'completed';
    this.todosService.Update(null, this.todoData).subscribe(
      (res: any) => {
        this.todoData = res;
      },
      () => {
        this.matSnackBar.open(
          'Error while Updating Task Please try again',
          '❌',
          {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }
        );
      }
    );
  }
  // Function detect the changes on the Sub-task
  onCheckboxChange(event: any, user: any) {
    if (event.target.checked) {
      this.selectedUsers.push(user.userId);
    } else {
      const index = this.selectedUsers.indexOf(user.userId);
      if (index >= 0) {
        this.selectedUsers.splice(index, 1);
      }
    }
  }
  // All Updates on the checklist
  changechecklist(i: any) {
    this.todoData.progress =
      this.todoData.progress + 100 / this.todoData.checkList.length > 100
        ? 100
        : this.todoData.progress + 100 / this.todoData.checkList.length;
    this.todoData.checkList[i].check = true;
    this.todoData.checkList[i].updated = new Date();
    this.todoData.checkList[i].userId = this.tokenStorage.getUser() as string;
    this.todoData.status == 'open'
      ? (this.todoData.status = 'inProgress')
      : null;
    this.todosService.Update(null, this.todoData).subscribe(
      (res: any) => {
        this.todoData = res;
      },
      () => {
        this.matSnackBar.open('Error while Updating Task', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  // Delete Sub-Task
  deleteCheck(i: any) {
   if(confirm("Are you sure you want to delete this subtask ??")){
    this.todoData.checkList.splice(i, 1);
    this.todosService.Update(null, this.todoData).subscribe(
      (res: any) => {
        this.todoData = res;
        this.resetProgress();
      },
      () => {
        this.matSnackBar.open('Error while Deleting Sub-Task', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
   }
  }
  // Switch Editing Mode
  switchCheck() {
    this.check = !this.check;
  }
  // Add new Sub-Task
  addCheck() {
    if(this.newCheck !=""){
      this.todoData.checkList?.length == null
      ? (this.todoData.checkList = [{ check: false, text: this.newCheck, updated : this.currentDate,userId : '' }])
      : this.todoData.checkList.push({ check: false, text: this.newCheck,updated : this.currentDate,userId : '' });
    this.todoData.progress =
      this.todoData.progress + 100 / this.todoData.checkList.length > 100
        ? 100
        : this.todoData.progress + 100 / this.todoData.checkList.length;
    this.todosService.Update(null, this.todoData).subscribe(
      (res: any) => {
        this.todoData = res;
        this.switchCheck();
        this.resetProgress();
        this.matSnackBar.open('new Sub-Task created', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      },
      () => {
        this.matSnackBar.open('Error while adding new Sub-Task', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
    this.newCheck = '';
    }else{
      this.matSnackBar.open(
        'Please enter a valid Sub-Task',
        '❌',
      )
    }
  }
  // Get Users Related to the current task
  getusers() {
    this.userService.get(this.todoData.supervisorId).subscribe(
      (res: UserEntity) => {
        this.superViser = res;
      },
      () => {
        this.matSnackBar.open(
          'Error while trying to get Superviser infos',
          '❌',
          {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }
        );
      }
    );
    this.todoData?.userIds.forEach((el: any) => {
      this.userService.get(el).subscribe(
        (res: UserEntity) => {
          this.taskMembers.push(res);
        },
        () => {
          this.matSnackBar.open('Error while trying to get users', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
    });
  }
  //Get all Team Members in cas the Team Leader want to Add a new User to the task
  getMembers() {
    this.projectMembers = [];
    this.projectService.get(this.todoData.projectId).subscribe(
      (res: any) => {
        res.team.usersId.forEach(
          (element: string) => {
            this.userService.get(element).subscribe((res: UserEntity) => {
              this.projectMembers.push(res);
            });
          },
          () => {
            this.matSnackBar.open('Error while trying to get users', '❌', {
              duration: 2000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });
          }
        );
      },
      () => {
        this.matSnackBar.open('Error while trying to get main Ptoject', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  // Update Task
  EditTask() {
    if (this.editTask == false) {
      this.editTask = true;
      this.getMembers();
    } else {
      if(this.todoData.name !="" && this.todoData.desc !=""){
        this.todoData.userIds = this.selectedUsers as [];
        this.todosService.Update(null, this.todoData).subscribe(
          (res: any) => {
            this.todoData = res;
            this.taskMembers = [];
            this.editTask = false;
            this.getusers();
          },
          () => {
            this.matSnackBar.open('Error while Updating Tasks', '❌', {
              duration: 2000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });
          }
        );
      }
    }
  }
  // Function that always control the progress of the Task
  resetProgress() {
    this.todoData.progress = 0;
    let i = 0;
    this.todoData.checkList.forEach((el: any) => {
      if (el.check == true) {
        i++;
      }
    });
    this.todoData.progress = (i * 100) / this.todoData.checkList.length;
    this.todosService.Update(null, this.todoData).subscribe(
      (res: any) => {
        this.todoData = res;
      },
      () => {
        this.matSnackBar.open('Error while Updating Task', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  //View CheckList Infos
  viewInfo(item : checkList){
    let userName ="";
    const date = new Date(item.updated);
    const formattedDate = date.toLocaleString();
    this.userService.get(item.userId).subscribe((res : UserEntity)=>{
      userName = res.firstname + ' ' + res.lastname;
      this.matSnackBar.open(
        'Validated at : ' + formattedDate + ' by ' + userName+"/ Sub-Task Name : "+item.text,
        '❌',
        {
          duration: 10000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        }
      );
    })

  }
}
