import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from 'src/app/Services/Teams/project.service';
import { UserService } from '../../../../Services/Soprahr/user.service';
import { TodosService } from 'src/app/Services/Teams/todos.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ManagerRole, TaskUpperRoles } from 'src/assets/roles.constants';
import { Project } from 'src/app/models/Project';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
const LogoImgPath =
  'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp';
@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['../../../../../assets/Project.css'],
})
export class ProjectDetailsComponent {
  id!: any;
  projectData: Project = new Project();
  folders: any[] = [];
  currentFolder: any;
  openTasks: any = [];
  inProgressTasks: any = [];
  completedTasks: any = [];
  inputGoals: boolean = false;
  inputProject: boolean = false;
  goals: string = '';
  newFolder = {
    folderName: '',
    goals: '',
    status: 'Open',
  };

  editProject() {
    this.inputProject = true;
  }

  userIds: any[] = [];

  projectMembers: any[] = [];

  users!: UserEntity[];
  selectedUsers: string[] = [];
  accessUser: boolean =
    ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
  accessUserTL: boolean =
    TaskUpperRoles.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
  newTask = {
    name: '',
    desc: '',
    priorty: '',
    userIds: ([] = []),
    endTime: new Date(),
    projectId: '',
    status: 'open',
    progress: 0,
    supervisorId: '',
    folderId: '',
  };
  constructor(
    private tokenStorage: TokenStorageService,
    public ar: ActivatedRoute,
    private projectService: ProjectService,
    private router: Router,
    private userService: UserService,
    private todoService: TodosService,
    private matSnackBar: MatSnackBar,
    private sanitizer: DomSanitizer
  ) {
    this.ar.params.subscribe((data) => {
      this.id = data['id'];
    });
    this.getProjectData();
  }
  checkUser() {
    if (this.accessUser || this.projectData.team === undefined) {
    } else if (
      this.projectData.team.usersId.indexOf(
        this.tokenStorage.getUser() as string
      ) !== -1
    ) {
    } else {
      this.matSnackBar.open("You don't have access to this project", '❌');
      setTimeout(() => {
        this.router.navigate(['/Dashboard/Project']);
      }, 500);
    }
  }
  //Get All Project Data
  getProjectData(id: any = this.id) {
    this.projectService.get(id).subscribe(
      (response) => {
        this.projectData = response;
        this.checkUser();
        this.folders = response.folder;
        this.currentFolder = response.folder[0];
        this.splitData(response.folder[0]);
      },
      () => {
        this.matSnackBar.open('Error while trying to get Project data', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  // Split the tasks By it's status
  splitData(item: any) {
    this.currentFolder = item;
    if (this.currentFolder.todos.length > 0) {
      this.openTasks = this.currentFolder.todos.filter(
        (todo: any) => todo.status === 'open'
      );
      this.inProgressTasks = this.currentFolder.todos.filter(
        (todo: any) => todo.status === 'inProgress'
      );
      this.completedTasks = this.currentFolder.todos.filter(
        (todo: any) => todo.status === 'completed'
      );
      this.currentFolder.pourcentage = Math.round(
        (100 * this.completedTasks.length) /
          (this.openTasks.length +
            this.inProgressTasks.length +
            this.completedTasks.length)
      );
      setTimeout(() => {
        this.openTasks.forEach((item: any) => {
          item.userIds.forEach((userId: string) => {
            this.fetchProfilePicture(userId);
          });
        });
        this.inProgressTasks.forEach((item: any) => {
          item.userIds.forEach((userId: string) => {
            this.fetchProfilePicture(userId);
          });
        });
        this.completedTasks.forEach((item: any) => {
          item.userIds.forEach((userId: string) => {
            this.fetchProfilePicture(userId);
          });
        });
      }, 50);
    } else {
      this.currentFolder.pourcentage = 0;
      this.openTasks = [];
      this.inProgressTasks = [];
      this.completedTasks = [];
    }
  }
  // Function to Edit Goal of the Folder
  editGoal() {
    this.inputGoals = true;
    this.goals = this.currentFolder.goals;
  }
  // add new Folder to the project
  addFolder() {
    if(this.newFolder.folderName !=""){
      this.projectService.addNewFloder(this.id, this.newFolder).subscribe(
        (res: any) => {
          window.location.reload();
        },
        () => {
          this.matSnackBar.open(
            'Error while trying to creating new Folder',
            '❌',
            {
              duration: 2000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            }
          );
        }
      );
    }else{
      this.matSnackBar.open(
        'Please Enter Folder Name',
        '❌',
      )
    }
  }
  // Updating a folder in dataBase
  updateFolder(status: any = this.currentFolder.status) {
    this.currentFolder.status = status;
    this.goals != ''
      ? (this.currentFolder.goals = this.goals)
      : (this.currentFolder.goals = this.currentFolder.goals);
    this.projectService.updateFolder(this.id, this.currentFolder).subscribe(
      (res: any) => {
        this.inputGoals = false;
      },
      () => {
        this.matSnackBar.open('Error while trying to Updating Folder', '❌', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }

  //Get users in case the Team Leader of Higher granted roles want to add new task
  getusers() {
    this.projectMembers = [];
    console.log(this.projectData)
    this.userIds = this.projectData.team.usersId;
    this.userIds.forEach((el: any) => {
      this.userService.get(el).subscribe(
        (res: UserEntity) => {
          this.projectMembers.push(res);
        },
        () => {
          this.matSnackBar.open('Error while trying to get Users', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
    });
  }

  //on Check the box get the selected Users
  onCheckboxChange(event: any, user: any) {
    if (event.target.checked) {
      this.selectedUsers.push(user.userId);
    } else {
      const index = this.selectedUsers.indexOf(user.userId);
      if (index >= 0) {
        this.selectedUsers.splice(index, 1);
      }
    }
  }

  // Save the new task
  saveTask() {
    if(this.newTask.name !="" && this.newTask.desc !=""){
      this.newTask.userIds = this.selectedUsers as [];
      this.newTask.folderId = this.currentFolder.folderNum;
      const userAuth = this.tokenStorage.getUser();
      this.newTask.supervisorId = userAuth as string;
      this.newTask.projectId = this.id;
      this.todoService.Create(this.newTask).subscribe(
        (res: any) => {
          this.getProjectData();
        },
        () => {
          this.matSnackBar.open('Error while Saving the Task in DataBase', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
    }else{
      this.matSnackBar.open(
        "Don't leave Empty fields",
        "Close"
      )
    }
  }

  // Delete a selected Task
  deleteTask(id: any) {
    if(confirm("Are you sure you want to delete this task ??")){
      this.todoService.Delete(id).subscribe(
        () => {
          this.getProjectData();
          this.matSnackBar.open('Task Deleted Successfully', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        },
        (error) => {
          this.matSnackBar.open('Error', 'Close');
        }
      );
    }
  }
  // Update Project infos
  updateProject() {
    if(this.projectData.projectName != ""&& this.projectData.projectDesc !=""){
      this.projectService.Update(this.projectData.id, this.projectData).subscribe(
        (res: any) => {
          this.inputProject = false;
          this.projectData = res;
          this.matSnackBar.open('Project Updated Successfully', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        },
        () => {
          this.matSnackBar.open('Error while Updating project', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
    }else{
      this.matSnackBar.open(
        "Don't leave Empty fields",
        "Close"
      )
    }
  }

  // get Profile Picture
  // Define a property to store the profile picture URLs
  userProfilePictures: { [userId: string]: SafeUrl } = {};

  // Call the function to fetch the profile picture for a user
  fetchProfilePicture(userId: string): void {
    this.userService.getFile(userId).subscribe(
      (res: any) => {
        let objectURL = URL.createObjectURL(res);
        this.userProfilePictures[userId] =
          this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      (error) => {
        this.userProfilePictures[userId] = LogoImgPath;
      }
    );
  }
}
