import { Component } from '@angular/core';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent {
  UsersList: UserEntity[] = [];
  totalPages = 0;
  currentPage = 1;
  UserNameFilter: string = '';
  ModalTitle: string = '';
  ActivateAddEditEmpComp: boolean = false;
  Role: RoleEntity = new RoleEntity(0, '', '');
  userModel: UserEntity = {
    userId: '',
    firstname: '',
    lastname: '',
    email: '',
    siteId: 0,
    siteName: '',
    departmentId: '',
    departmentDomain: '',
    roleId: '',
    role: this.Role,
  };
  UserListWithoutFilter: UserEntity[] = [];
  constructor(
    private service: UserService,
    private MatSnackBar: MatSnackBar
  ){}
  
  ngOnInit(): void {
    this.refreshList();
  }

  // Get All users
  refreshList(page: number = 1, limit: number = 5) {
    this.service.getAllWithpage(page, limit).subscribe(
      (data) => {
        this.UsersList = data.userDtoList;
        this.UserListWithoutFilter = data.userDtoList;
        this.totalPages = data.totalPages;
      },
      () => {
        this.MatSnackBar.open('Error while loading data', '❌', {
          duration: 3000,
        });
      }
    );
  }
  // Open Model on Adding new user
  addClick() {
    this.userModel = {
      userId: '',
      firstname: '',
      lastname: '',
      email: '',
      siteId: 0,
      siteName: '',
      departmentId: '',
      departmentDomain: '',
      roleId: '',
      role: {
        idRole: 0,
        roleName: '',
        job_title: '',
      },
    };
    this.ModalTitle = 'add User';
    this.ActivateAddEditEmpComp = true;
  }
  // Open Model to change some of user informations
  EditClick(item: any) {
    this.userModel = item;
    this.ModalTitle = 'Edit User';
    this.ActivateAddEditEmpComp = true;
  }
  // Delete the selected user From Database
  DeleteClick(item: any) {
    if (confirm('are u sure ??')) {
      this.service.Delete(item.userId).subscribe((data) => {
        this.refreshList();
      });
    }
  }
  // Function to filter all users By thier Email
  FilterFn() {
    var UserNameFilter = this.UserNameFilter;
    this.UsersList = this.UserListWithoutFilter.filter(function (el: any) {
      return el.email
        .toString()
        .toLowerCase()
        .includes(UserNameFilter.toString().trim().toLowerCase());
    });
  }
  // Sort User List According To emails Letters
  sortResult(prop: any, asc: any) {
    this.UsersList = this.UserListWithoutFilter.sort(function (a: any, b: any) {
      if (asc) {
        return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
      } else {
        return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
      }
    });
  }
  //Close the Model
  closeClick() {
    this.ActivateAddEditEmpComp = false;
    this.refreshList();
  }
  // Parse throw all pages
  Parser(totalPages: any) {
    let pages = [];
    for (let i = 1; i <= totalPages; i++) {
      pages.push(i);
    }
    return pages;
  }
  // Move to Next page
  moveNextPage() {
    if (this.currentPage < this.totalPages) {
      this.currentPage++;
      this.refreshList(this.currentPage);
    }
  }
  // Move to the privouis Page
  moveBackPage() {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.refreshList(this.currentPage);
    }
  }
  // Move to a Chosen page
  moveToPage(page: number) {
    this.currentPage = page;
    this.refreshList(this.currentPage);
  }
  // Change the Amount of users Per Page
  onChangeNumberUsersPerPage(number: any) {
    this.refreshList(1, number);
  }
}
