import { Component, Input } from '@angular/core';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RoleService } from '../../../../Services/Soprahr/role.service';
import { SiteService } from 'src/app/Services/Soprahr/site.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';
import { SiteEntity } from 'src/app/models/SopraModels/SiteEntity';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-edit-users',
  templateUrl: './add-edit-users.component.html',
  styleUrls: ['./add-edit-users.component.css'],
})
export class AddEditUsersComponent {
  superviserIdChoosen : string = "";
  Role: RoleEntity = new RoleEntity(0, '', '');
  @Input() user: UserEntity = {
    userId: '',
    firstname: '',
    lastname: '',
    email: '',
    siteId: 0,
    siteName: '',
    departmentId: '',
    departmentDomain: '',
    roleId: '',
    role: this.Role,
  };
  listRoles : RoleEntity[] = [];
  listSites : SiteEntity[] = [];
  listUsers : UserEntity[] = [];
  constructor(
    private _snackBar: MatSnackBar,
    private service: UserService,
    private userInfoService: UserInfoService,
    private roleCervice: RoleService,
    private siteService: SiteService,
    private tokenStorageService: TokenStorageService,
    private router: Router
  ) {
    this.getUsers()
  }

  ngOnInit(): void {
    this.user = this.CreateModel();
    this.getRoles();
    this.getSite();
  }

  //Create Model
  CreateModel() {
    return new UserEntity(
      this.user.userId,
      this.user.firstname,
      this.user.lastname,
      this.user.email,
      this.user.siteId,
      this.user.siteName,
      this.user.departmentId,
      this.user.departmentDomain,
      this.user.roleId,
      this.user.role
    );
  }
  // Create User In dataBase
  CreateUser() {
    if(this.user.firstname !='' && this.user.lastname !=''&& this.user.email !=''){
      var newUser = {
        firstname: this.user.firstname,
        lastname: this.user.lastname,
        email: this.user.email,
        password: 'SopraHR2023++',
        roleId: this.user.role.idRole,
        siteId: this.user.siteId,
      };

      this.service.Create(newUser).subscribe(
        (res: UserEntity) => {
          var newUserInfo = {
            userId: res.userId,
            skills: [],
            skillLevel: '0',
            interstLevel: '0',
            competencyLevel: '0',
            superViserId: this.superviserIdChoosen !="" ? this.superviserIdChoosen : this.tokenStorageService.getUser() as string,
          };
          this.userInfoService.Create(newUserInfo).subscribe(() => {
            window.location.reload();
          });
        },
        (error) => {
          this._snackBar.open(
            'While Creating User We got this ERROR' + error.message,
            '❌'
          );
        }
      );
    }else{
      this._snackBar.open(
        'Please Fill All The Fields',
        '❌'
      );
    }
  }
  // Edit User
  EditUser() {
    var updatedUser = {
      userId: this.user.userId,
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      email: this.user.email,
      roleId: this.user.role.idRole,
      siteId: this.user.siteId,
      departmentId: this.user.departmentId,
    };
    this.service.Update(this.user.userId, updatedUser).subscribe(
      () => {
        window.location.reload();
      },
      (error) => {
        this._snackBar.open(
          'While Updating User We got this ERROR' + error.message,
          '❌'
        );
      }
    );
  }
  // Get All roles for the Roles List to Assing user a Role
  getRoles() {
    this.roleCervice.getAll().subscribe(
      (roles) => {
        this.listRoles = roles;
      },
      (error) => {
        this._snackBar.open(
          'While Getting Roles We got this ERROR' + error.message,
          '❌'
        );
      }
    );
  }
  // Get All sites to assing user A Site
  getSite() {
    this.siteService.getAll().subscribe(
      (sites) => {
        this.listSites = sites;
      },
      (error) => {
        this._snackBar.open(
          'While Getting Sites We got this ERROR' + error.message,
          '❌'
        );
      }
    );
  }
  // Dismiss Model
  CloseModel() {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/Dashboard/Users']);
    });
  }
  getUsers(){
    this.service.getAll().subscribe((res :any)=>{
      this.listUsers = res.userDtoList;
      console.log(res);
    })
  }
}
