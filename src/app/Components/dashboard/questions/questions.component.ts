import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SiteService } from 'src/app/Services/Soprahr/site.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserInfo } from 'src/app/models/UserInfo';
import { QuestionService } from 'src/app/Services/Skill/question.service';
@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent {
  public name: string = "";
  showExplaination : boolean = false;
  public questionList: any = [];
  public currentQuestion: number = 0;
  public points: number = 0;
  counter = 60;
  correctAnswer: number = 0;
  inCorrectAnswer: number = 0;
  interval$: any;
  progress: string = "0";
  isQuizCompleted : boolean = false;
  id!:any;
  constructor(private router : Router, private tokenStorage : TokenStorageService ,private http : HttpClient,public ar: ActivatedRoute,private userInfoService : UserInfoService,private questionService : QuestionService) {
    this.ar.params.subscribe(data => {this.id = data['id'];});

  }
  ngOnInit(): void {
    this.name = localStorage.getItem("name")!;
    this.getAllQuestions();
    this.startCounter();
  }
  getAllQuestions() {
     this.questionService.get(this.id).subscribe((res : any)=>{
        this.questionList = res;
      })
  }
  nextQuestion() {
    this.currentQuestion++;
  }
  previousQuestion() {
    this.currentQuestion--;
  }
  SaveData(){
    const userAuth = this.tokenStorage.getUser();
    this.userInfoService.get(userAuth).subscribe((res : UserInfo)=>{
      let data = res;
      data.skills.forEach((skill:any) => {
        if(skill.skillId == this.id){
          skill.testToken = true;
          skill.scroce = this.points;
        }
      })

      //lezmny naaml model w nekho fyh kol chy w baad bch najem naaml set lel avg  w score w zeby
      this.userInfoService.Update(this.id,data).subscribe((res:any)=>{
        setTimeout(() => {
          this.router.navigate(['/Dashboard/Profile'])
        }, 2000);

      });
    })
  }
  answer(currentQno: number, option: any) {
    this.showExplaination = true;

    if(currentQno === this.questionList.length){
      this.isQuizCompleted = true;
      this.stopCounter();
      this.SaveData();

    }
    if (option.correct) {
      this.points += 100/this.questionList.length;
      this.correctAnswer++;
      setTimeout(() => {
        this.currentQuestion++;
        this.resetCounter();
        this.getProgressPercent();
        this.showExplaination = false;
      }, 3000);
    } else {
      setTimeout(() => {
        this.currentQuestion++;
        this.inCorrectAnswer++;
        this.resetCounter();
        this.getProgressPercent();
        this.showExplaination = false;
      }, 3000);
    }
  }
  startCounter() {
    this.interval$ = interval(1000)
      .subscribe(val => {
        this.counter--;
        if (this.counter === 0) {
          this.currentQuestion++;
          this.counter = 60;
          this.points -= 10;
        }
      });
    setTimeout(() => {
      this.interval$.unsubscribe();
    }, 600000);
  }
  stopCounter() {
    this.interval$.unsubscribe();
    this.counter = 0;
  }
  resetCounter() {
    this.stopCounter();
    this.counter = 60;
    this.startCounter();
  }
  getProgressPercent() {
    this.progress = ((this.currentQuestion / this.questionList.length) * 100).toString();
    return this.progress;

  }
}

