import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { ProjectService } from 'src/app/Services/Teams/project.service';
import { TodosService } from 'src/app/Services/Teams/todos.service';
import * as echarts from 'echarts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../../../../assets/style.bundle.css'],
})
export class HomeComponent {
  todoList: any[] = [];
  checkList: any[] = [];
  projectsList: any[] = [];
  completedTasks: number = 0;
  userId: string = '';

  // For Sub-Tasks Diagramm
  chartInstance: any;
  dataSubTasks: any[] = [];
  optionSubTasks: any;

  // For Skills Diagramm
  optionSkills: any;
  userSkillData: any = {};
  SkilldataDiagram: any = {
    skillName: [],
    nbrSkills: ([] = [0]),
    avgLevel: [],
    SkillLevel: [],
  };

  // For Skills Category
  optionCategory: any;
  CategoryData: any = {
    indicator: [],
    data: [
      {
        value: [],
        name: 'Required Level',
      },
      {
        value: [],
        name: 'Actual Level',
      },
    ],
  };
  constructor(
    private todoService: TodosService,
    private tokenStorage: TokenStorageService,
    private projectService: ProjectService,
    private userInfoService: UserInfoService,
    private skillService: SkillService,
    private matSnackBar: MatSnackBar
  ) {
    this.userId = this.tokenStorage.getUser() as string;
    this.getTodosByUser();
    this.getProjectByUser();
    this.getChecklists();
    this.createSkillsChart();
    this.createCategoryChart();
  }

  // For Sub-Tasks Diagramm
  getChecklists() {
    this.todoService.getCheckListsByUserId(this.userId).subscribe((res) => {
      this.checkList = res;
      this.dataSubTasks[0] = 0;
      this.dataSubTasks[1] = res[0].length;
      this.dataSubTasks[2] = res[1].length;
      this.dataSubTasks[3] = res[2].length;
      this.dataSubTasks[4] = res[3].length;
      this.dataSubTasks[5] = res[4].length;
      this.dataSubTasks[6] = res[5].length;
      this.createChartTasks();
    });
  }
  createChartTasks() {
    this.optionSubTasks = {
      title: {
        text: 'Preformance/m',
      },
      tooltip: {
        trigger: 'axis',
      },
      legend: {},
      toolbox: {
        show: true,
        feature: {
          magicType: { type: ['line', 'bar'] },
          saveAsImage: {},
        },
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['0', '5', '10', '15', '20', '25', '30'],
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          formatter: '{value} SubT',
        },
      },
      series: [
        {
          name: 'Sub-Tasks Completed',
          type: 'line',
          data: this.dataSubTasks,
          markLine: {
            data: [{ type: 'average', name: 'Avg' }],
          },
        },
      ],
    };
  }
  onChartEvent($event: any) {
    setTimeout(() => {
      const result = this.chartInstance.getModel().getSeries();
    }, 50);
    let message = '';
    for (const item of this.checkList[$event.dataIndex - 1]) {
      message += `- ${item.text} \n`;
    }
    if ($event.dataIndex != 0) {
      this.matSnackBar.open(
        `You have completed from ${($event.dataIndex - 1) * 5} to ${
          $event.dataIndex * 5
        } days ${$event.data} tasks : ` + message,
        '❌',
        {
          duration: 8000,
          panelClass: ['green-snackbar'],
          horizontalPosition: 'center',
          verticalPosition: 'top',
        }
      );
    }
  } // On Chart Inisialization
  onChartInit(e: any) {
    this.chartInstance = e;
  }
  // For Skills Diagramm
  createSkillsChart() {
    this.userInfoService.get(this.userId).subscribe((res: any) => {
      this.userSkillData = res;
      this.userSkillData?.skills.forEach((skill: any) => {
        if (
          !this.SkilldataDiagram.skillName.some(
            (element: any) => element == skill?.skillName
          )
        ) {
          this.skillService.getSkill(skill?.skillId).subscribe((res: any) => {
            this.SkilldataDiagram.SkillLevel.push(Number(res.level));
          });
          this.SkilldataDiagram.skillName.push(skill?.skillName);
          this.SkilldataDiagram.nbrSkills.push(
            this.SkilldataDiagram.nbrSkills[
              this.SkilldataDiagram.nbrSkills.length - 1
            ] + 1
          );
          this.SkilldataDiagram.avgLevel.push(skill?.avgLevel);
        } else {
          this.SkilldataDiagram.avgLevel[
            this.SkilldataDiagram.skillName.indexOf(skill?.skillName)
          ] =
            (this.SkilldataDiagram.avgLevel[
              this.SkilldataDiagram.skillName.indexOf(skill?.skillName)
            ] +
              skill?.avgLevel) /
            2;
        }
      });
    });
    setTimeout(() => {
      this.SkilldataDiagram.nbrSkills.pop();
      this.optionSkills = {
        title: {
          text: 'Skills',
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#283b56',
            },
          },
        },
        legend: {},
        toolbox: {
          show: true,
          feature: {
            dataView: { readOnly: false },
            restore: {},
            saveAsImage: {},
          },
        },
        dataZoom: {
          show: false,
          start: 0,
          end: 100,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: true,
            data: this.SkilldataDiagram.skillName,
          },
          {
            type: 'category',
            boundaryGap: true,
            data: this.SkilldataDiagram.nbrSkills,
          },
        ],
        yAxis: [
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2],
          },
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2],
          },
        ],
        series: [
          {
            name: 'Actual Avg Level',
            type: 'bar',
            xAxisIndex: 1,
            yAxisIndex: 1,
            data: this.SkilldataDiagram.avgLevel,
          },
          {
            name: 'Target',
            type: 'line',
            data: this.SkilldataDiagram.SkillLevel,
          },
        ],
      };
    }, 350);
  }

  // For the Category Chart
  createCategoryChart() {
    this.CategoryData = {
      indicator: [],
      data: [
        {
          value: [],
          name: 'Required Level',
        },
        {
          value: [],
          name: 'Actual Level',
        },
      ],
    };
    this.userInfoService
      .getCategoriesByUser(this.userId)
      .subscribe((res: any) => {
        Object.keys(res).forEach((el: any) => {
          this.CategoryData.indicator.push({ name: el, max: 10 });
          this.CategoryData.data[0].value.push(res[el][0]);
          this.CategoryData.data[1].value.push(res[el][1]);
          console.log(res);
        });
      });
    setTimeout(() => {
      this.optionCategory = {
        title: {
          text: 'My Categories',
        },
        legend: {
          data: ['Actual', 'Target'],
        },
        radar: {
          shape: 'circle',
          indicator: this.CategoryData.indicator,
        },
        series: [
          {
            name: 'Actual vs Target',
            type: 'radar',
            data: this.CategoryData.data,
          },
        ],
      };
    }, 100);
  }
  //Get All Tasks related to the current Authenticated user
  getTodosByUser() {
    this.todoService
      .getTodosByUserId(this.tokenStorage.getUser() as string)
      .subscribe(
        (res: any) => {
          this.todoList = res;
          this.completedTasks = this.todoList.filter(
            (todo: any) => todo.status === 'completed'
          ).length;
          this.todoList = this.todoList.filter(
            (todo: any) => todo.status === 'inProgress'
          );
        },
        () => {
          this.matSnackBar.open('Error while trying to get Tasks', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
  }
  //Get All projects belong to the current authenticated user
  getProjectByUser() {
    this.projectService
      .getProejctByUserId(this.tokenStorage.getUser() as string)
      .subscribe(
        (res: any) => {
          this.projectsList = res;
        },
        () => {
          this.matSnackBar.open('Error while trying to get Projects', '❌', {
            duration: 2000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
  }
  //Count the remaining CheckLists
  countCheckList(item: any) {
    let count = 0;
    if (item.checkList) {
      item.checkList.forEach((check: any) => {
        if (check.check == true) {
          count++;
        }
      });
      return count + '/' + item.checkList.length;
    } else {
      return '0 /0';
    }
  }
}
