import { Component } from '@angular/core';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserInfo } from 'src/app/models/UserInfo';
import { Skill } from 'src/app/models/skill';
import { CategoryService } from './../../../Services/Skill/category.service';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ManagerRole } from 'src/assets/roles.constants';

export class Categories{
  categoryName : string = '';
  skills : string[] = [];
}
@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.css']
})
export class AssessmentComponent {
  UserInfoData : any = new UserInfo();
  skill !: Skill;
  userSkillInfo !:any;
  skillId :string = "";
  userId :string ="";
  value : number =0;
  assessing : boolean = false;
  listSkills : any[] = [];
  accessUser : boolean = ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;

  constructor(private router : Router,public ar: ActivatedRoute,private userInfoService : UserInfoService, private tokenStorage : TokenStorageService,private categoryService :CategoryService ,private skillService : SkillService){
    this.ar.queryParams
    .subscribe(params => {
      this.assessing = params['assessing'];
      if(this.assessing && this.accessUser){
        this.ar.params.subscribe(data => {this.userId = data['id'];
        this.getUserAllSkill();
      });
      }
    });
    if(!this.assessing){
      this.ar.params.subscribe(data => {
        this.skillId = data['id'];
        this.getSkillForSelfAssessment();
      });
    }

  }
  getUserAllSkill(){
    this.userInfoService.get(this.userId).subscribe(data => {
      this.userSkillInfo = data;
      this.userSkillInfo.skills.forEach((element : any) => {
        this.listSkills.push({supervisorAssignment : element.supervisorAssignment , skillName : element.skillName});
  })})
}


  getSkillForSelfAssessment(){
    this.assessing = false;
    let cat = new Categories();
    this.skillService.get(this.skillId).subscribe((res)=>{
      cat.categoryName = res.categoryName;
      this.skill = res;
    })
    this.userInfoService.get(this.tokenStorage.getUser()).subscribe((res)=>{
      this.UserInfoData = res;
      this.UserInfoData.skills.forEach((skill : any) => {
        if(skill.skillId == this.skillId){
          this.userSkillInfo = skill;
          this.value = skill.selfAssignment;
        }
      });
    });
  }
  onSubmit() {
    console.log(this.skillId)
    this.UserInfoData.skills.forEach((element : any) => {
      if(element.skillId == this.skillId){
        element.selfAssignment = this.value;
      }
    });
    this.userInfoService.putUserAferSelfAssessment(this.skillId,this.UserInfoData)
      .subscribe((res : any)=>{
        this.router.navigate(['/Dashboard/Profile/Assessment']);
    })
  }

  onSuperviserSubmit(){
    console.log(this.listSkills);
    this.userSkillInfo.skills.forEach((element : any) => {
      this.listSkills.forEach((skill : any)=>{
        if(element.skillName == skill.skillName){
          element.supervisorAssignment = skill.supervisorAssignment;
          }
      })
    });
    this.userInfoService.putUserAfterSuperviserAssessment(this.userSkillInfo).subscribe((res : any)=>{
      this.router.navigate(['/Dashboard/SuperViser/Assess']);
    })
  }

}
