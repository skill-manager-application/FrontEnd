import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAssessmentComponent } from './profile-assessment.component';

describe('ProfileAssessmentComponent', () => {
  let component: ProfileAssessmentComponent;
  let fixture: ComponentFixture<ProfileAssessmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileAssessmentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProfileAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
