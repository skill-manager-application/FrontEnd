import { Component } from '@angular/core';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {  ManagerRole } from 'src/assets/roles.constants';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-assessment',
  templateUrl: './profile-assessment.component.html',
  styleUrls: ['../../../../../assets/style.bundle.css'],
})
export class ProfileAssessmentComponent {
  userInfo!: any;
  userSkillAssessmentNotToken: any[] = [];
  option: boolean = true;
  currentDate = new Date();
  maxDate: Date = new Date();
  superviser: string = '';
  lastCompleted: Date = new Date();
  missingAssessments: number = 0;
  missingSelfAssessment : number =0;
  accessUser : boolean = ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
  outerProfile : boolean = false;

  constructor(
    private route: ActivatedRoute,
    private userinfoService: UserInfoService,
    private tokenStorageService: TokenStorageService,
    private userService: UserService,
    private MatSnackBar: MatSnackBar,
    private tokenStorage : TokenStorageService
  ) {
    this.route.parent?.params.subscribe(params => {
      if( params['id'] !== undefined){
        this.outerProfile = true;
        this.refreshProfile(params['id']);
      }else{
        this.outerProfile = false;
        this.refreshProfile(this.tokenStorageService.getUser() as string)
      }
    });

  }

  refreshProfile(id : string){
    this.userinfoService.get(id).subscribe(
      (res: any) => {
        this.userInfo = res;
        this.getSkills();
        this.getSuperViser(this.userInfo.superViserId);
        this.getLastCompleted();
      },
      () => {
        this.MatSnackBar.open('Error while loading data', '❌', {
          duration: 3000,
        });
      }
    );
  }
  // Get User Superviser
  getSuperViser(userId: string) {
    this.userService.get(userId).subscribe(
      (res: UserEntity) => {
        this.superviser = res.lastname + ' ' + res.firstname;
        this.getSuperviserMissingAssessment();
      },
      (error) => {
        this.MatSnackBar.open("Error while loading superviser's Data", '❌', {
          duration: 3000,
        });
      }
    );
  }
  // Get Last Self Assignment Of the user
  getLastCompleted() {
    let lastCompletedDate!: Date;
    this.userInfo.skills.forEach((element: any) => {
      if (element.assessmentToken) {
        const passedAtDate = new Date(element.passedAt);
        passedAtDate.setHours(0, 0, 0, 0);
        if (!lastCompletedDate || passedAtDate > lastCompletedDate) {
          lastCompletedDate = passedAtDate;
        }
      }else{
        this.missingSelfAssessment ++;
      }
    });

    if (lastCompletedDate) {
      this.lastCompleted = lastCompletedDate;
    } else {
      // Handle the case when no skill with assessmentToken is found
      this.lastCompleted = new Date();
    }
  }

  // Get Skills From userIndo Data
  getSkills() {
    if (this.option) {
      this.userSkillAssessmentNotToken = this.userInfo.skills;
      this.option = false;
    } else {
      this.userSkillAssessmentNotToken = this.userInfo.skills.filter(
        (el: any) => !el.assessmentToken
      );
      this.option = true;
    }
  }
  // Get how many missng days fron the current date
  getDate(date: Date) {
    var date1 = new Date(date);
    date1.setHours(0, 0, 0, 0);
    this.currentDate.setHours(0, 0, 0, 0);
    var g1 = this.currentDate.toISOString();
    var g2 = date1.toISOString();
    if (g1 < g2)
     {
      if(date1.getMonth() - this.currentDate.getMonth() > 0){
        return (
          'Remaining ' +
          (date1.getMonth() - this.currentDate.getMonth()) +
          ' Months'
        );
      }
      else{
        return (
          'Remaining ' +
          (date1.getDay() - this.currentDate.getDay()) +
          ' Days'
        );
      }
     }
    else if (g1 > g2) {
      if (date1 > this.maxDate) {
        this.maxDate = date1;
      }
      return (
        'Over Due Late by' +
        (this.currentDate.getDate() - date1.getDate()) +
        ' Days'
      );
    } else return 'Soon';
  }

  // Get the superviser missing assessment of the user
  getSuperviserMissingAssessment() {
    this.userInfo.skills.forEach((element: any) => {
      if (
        element.supervisorAssignment == 0 ||
        element.supervisorAssignment == null
      ) {
        this.missingAssessments = this.missingAssessments + 1;
      }
    });
  }
}
