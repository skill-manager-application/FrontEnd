import { Component, Input } from '@angular/core';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { DepartmentService } from '../../../../Services/Soprahr/department.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { HigherGrantedRoles } from '../../../../../assets/roles.constants';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['../../../../../assets/style.bundle.css'],
})
export class OverviewComponent {
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private userInfoService : UserInfoService,
    private tokenStorage: TokenStorageService,
    private departmentService: DepartmentService,
    private MatSnackBar: MatSnackBar
  ) { this.route.parent?.params.subscribe(params => {
    if( params['id'] !== undefined){
      this.userId = params['id'] as string;
      this.outerProfile = true;
      this.outerProfile = !this.accessUser;
      this.refreshProfile()
    }else{
      this.userId = this.tokenStorage.getUser() as string;
      this.outerProfile = false;
      this.refreshProfile()
    }
  });
}





outerProfile : boolean = false;
userId : string ="";
  role: RoleEntity = new RoleEntity(0, '', '');
  userData: UserEntity = {
    userId: '',
    firstname: '',
    lastname: '',
    email: '',
    siteId: 0,
    siteName: '',
    departmentId: '',
    departmentDomain: '',
    roleId: '',
    role: this.role,
  };
  userInfo !: any;
  editUser: boolean = false;
  listDepartments: any[] = [];
  accessUser : boolean = HigherGrantedRoles.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
  option !: any;
  ngOnInit(): void {

  }
  // Get User Infos
  refreshProfile() {
    this.userService.get(this.userId).subscribe(
      (res: UserEntity) => {
        this.userData = res;
      },
      (error) => {
        this.MatSnackBar.open('Error while loading user Infos', 'Close', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
    this.userInfoService.get(this.userId).subscribe((res : any)=>{
        this.userInfo = res;
        this.createDataForChart();
      },
      (error)=>{
        this.MatSnackBar.open('Error while loading user Infos', 'Close', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
          });
    });

  }

  //get Departments in user's site in case he want to change it
  getDepartments() {
    this.departmentService.getDepartmentBySite(this.userData.siteId).subscribe(
      (res: any) => {
        this.listDepartments = res;
      },
      (error) => {
        this.MatSnackBar.open('Error while loading Departments', 'Close', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }

  // Edit User Infos
  editProfile() {
    if (this.editUser == false) {
      this.getDepartments();
      this.editUser = true;
    } else {
      if(this.userData.firstname != '' && this.userData.lastname != ''){
        console.log(this.userData.firstname,this.userData.departmentId )
        this.editUser = false;
        var updatedUser = {
          userId: this.userData.userId,
          firstname: this.userData.firstname,
          lastname: this.userData.lastname,
          email: this.userData.email,
          roleId: this.userData.role.idRole,
          siteId: this.userData.siteId,
          departmentId: this.userData.departmentId,
        };
        this.userService.Update(this.userData.userId, updatedUser).subscribe(
          () => {
            this.MatSnackBar.open('Yuor profile updated successfully', 'Close', {
              duration: 600,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });
            setTimeout(() => {
              window.location.reload();
            }, 500);
          },
          (error) => {
            this.MatSnackBar.open('Error while updating Profile', 'Close', {
              duration: 2000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });
          }
        );
      }else{
        this.MatSnackBar.open("Please don't leave fileds empty", 'Close', {
          duration: 1000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    }
  }

  createDataForChart(){
    this.option  = {
      tooltip: {
        formatter: '{a} <br/>{b} : {c}%'
      },
      series: [
        {
          name: 'Pressure',
          type: 'gauge',
          progress: {
            show: true
          },
          detail: {
            valueAnimation: true,
            formatter: '{value}'
          },
          data: [
            {
              value: Math.round(this.userInfo?.skillLevel *10),
              name: 'SCORE'
            }
          ]
        }
      ]
    };
  }
}
