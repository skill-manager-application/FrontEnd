import { Component } from '@angular/core';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { HigherGrantedRoles, ManagerRole } from 'src/assets/roles.constants';
const LogoImgPath =
  'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['../../../../assets/style.bundle.css'],
})
export class ProfileComponent {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private tokenStorage: TokenStorageService,
    private sanitizer: DomSanitizer,
    private MatSnackBar: MatSnackBar
  ) {
    this.route.params.subscribe(params => {
      if( params['id'] !== undefined){
        this.userId = params['id'] as string;
        this.outerProfile = true;
        this.refreshProfile()
      }else{
        this.userId = this.tokenStorage.getUser() as string;
        this.outerProfile = false;
        this.refreshProfile()
      }
    });
  }
  accessUser : boolean = ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;

  outerProfile : boolean = false;
  userId : string ="";
  role: RoleEntity = new RoleEntity(0, '', '');
  userData: UserEntity = {
    userId: '',
    firstname: '',
    lastname: '',
    email: '',
    siteId: 0,
    siteName: '',
    departmentId: '',
    departmentDomain: '',
    roleId: '',
    role: this.role,
  };
  imageProfile!: SafeUrl;
  uploadedImage!: any;

  ngOnInit(): void {
  }

  // Get user From SessionStorage in the browser
  refreshProfile() {
    this.getImage(this.userId as string);
    this.userService.get(this.userId ).subscribe(
      (res: UserEntity) => {
        this.userData = res;
      },
      () => {
        this.MatSnackBar.open('Error while loading user Infos', 'Close', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  // get Profile Picture
  getImage(userId: string) {
    this.userService.getFile(userId).subscribe(
      (res: any) => {
        let objectURL = URL.createObjectURL(res);
        this.imageProfile = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      () => {
        this.imageProfile = LogoImgPath;
      }
    );
  }

  // Upload Image
  onImageUpload(event: any) {
    this.uploadedImage = event.target.files[0];
    this.imageUploadAction();
  }
  // change profile picture
  imageUploadAction() {
    const imageFormData = new FormData();
    imageFormData.append('file', this.uploadedImage);
    this.userService.uploadImage(this.userId as string, imageFormData).subscribe(
      (response: any) => {
        console.log(response.status);
        this.getImage(this.userId as string);
        window.location.reload();
      },
      (error) => {
        this.MatSnackBar.open('Error while uploading your Image', 'Close', {
          duration: 2000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
  }
  goToPage(path : string) {
    console.log("/Dashboard/Profile/"+path)
    if (this.outerProfile) {
      this.router.navigate(['/Dashboard/Profile',this.userId,path]);
    } else {
      this.router.navigate(['/Dashboard/Profile',path]);
    }
  }
}
