import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['../../../../../assets/style.bundle.css']
})
export class SkillsComponent {
  constructor(private route: ActivatedRoute,private userdata : UserService,private tokenStorage : TokenStorageService,
    private userInfoService :UserInfoService,
    private skillService :SkillService,public router : Router)
    { this.route.parent?.params.subscribe(params => {
      if( params['id'] !== undefined){
        console.log(params['id'])
        this.userId = params['id'] as string;
        this.outerProfile = true;
        this.createData()
      }else{
        this.userId = this.tokenStorage.getUser() as string;
        this.outerProfile = false;
        this.createData()
      }
    });
  }
  outerProfile : boolean = false;
  userId : string ="";
  userSkillData : any = {};
  dataDiagram : any = {
    skillName : [],
    nbrSkills : [] = [0],
    avgLevel : [],
    SkillLevel : [],
  };
  mode : boolean = true;
  option : any;
  optionDynamic:any;
  RadarData : any = {
    indicator : [],
    data: [
      {
        value: [],
        name: 'Required Level'
      },
      {
        value: [],
        name: 'Actual Level'
      }
    ]
  }
  ngOnInit(): void {
  }

  // ddiagram chart
  createData(){
    this.userInfoService.get(this.userId).subscribe((res :any)=>{
    this.userSkillData = res;
    this.userSkillData?.skills.forEach((skill :any)=>{
      if(!this.dataDiagram.skillName.some((element :any) => element == skill?.skillName)){
        this.skillService.getSkill(skill?.skillId).subscribe((res :any)=>{
          this.dataDiagram.SkillLevel.push(Number(res.level));
        })
        this.dataDiagram.skillName.push(skill?.skillName);
        this.dataDiagram.nbrSkills.push(this.dataDiagram.nbrSkills[this.dataDiagram.nbrSkills.length-1]+1);
        this.dataDiagram.avgLevel.push(skill?.avgLevel);
      }else{
        this.dataDiagram.avgLevel[this.dataDiagram.skillName.indexOf(skill?.skillName)] = (this.dataDiagram.avgLevel[this.dataDiagram.skillName.indexOf(skill?.skillName)] + skill?.avgLevel)/2;
      }

    });
  })
    setTimeout(() => {
      this.dataDiagram.nbrSkills.pop();
      this.optionDynamic = {
        title: {
          text: 'component Assembly'
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#283b56'
            }
          }
        },
        legend: {},
        toolbox: {
          show: true,
          feature: {
            dataView: { readOnly: false },
            restore: {},
            saveAsImage: {}
          }
        },
        dataZoom: {
          show: false,
          start: 0,
          end: 100
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: true,
            data: this.dataDiagram.skillName
          },
          {
            type: 'category',
            boundaryGap: true,
            data: this.dataDiagram.nbrSkills
          }
        ],
        yAxis: [
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2]
          },
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2]
          }
        ],
        series: [
          {
            name: 'Actual Avg Level',
            type: 'bar',
            xAxisIndex: 1,
            yAxisIndex: 1,
            data: this.dataDiagram.avgLevel
          },
          {
            name: 'Target',
            type: 'line',
            data: this.dataDiagram.SkillLevel
          }
        ]
      };

    }, 150);
  }

  changeMode(){
    if(this.mode == true){
      this.mode = false;
      this.cat();
    }
    else{
      this.mode = true;
    }
  }

// radar chart
  cat(){
    this.RadarData = {
      indicator : [],
      data: [
        {
          value: [],
          name: 'Required Level'
        },
        {
          value: [],
          name: 'Actual Level'
        }
      ]
    }
    this.userInfoService.getCategoriesByUser(this.userId).subscribe((res : any)=>{
     Object.keys(res).forEach((el : any)=>{
      this.RadarData.indicator.push({name : el , max : 10});
      this.RadarData.data[0].value.push(res[el][0]);
      this.RadarData.data[1].value.push(res[el][1]);
      console.log(res);
     });
    })
   setTimeout(() => {
    this.option = {
      legend: {
        data: ['Actual', 'Target']
      },
      radar: {
        shape: 'circle',
        indicator: this.RadarData.indicator
      },
      series: [
        {
          name: 'Actual vs Target',
          type: 'radar',
          data: this.RadarData.data
        }
      ]
    };
   }, 100);
  }
}
