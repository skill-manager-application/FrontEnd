import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { TeamService } from './../../../../Services/Teams/team.service';
import { Teams } from 'src/app/models/Teams';
import { UserInfo } from 'src/app/models/UserInfo';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { ManagerRole, TaskUpperRoles } from '../../../../../assets/roles.constants';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { Project } from 'src/app/models/Project';
import { ProjectService } from './../../../../Services/Teams/project.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
const LogoImgPath =
  'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp';
@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css'],
})
export class TeamDetailsComponent {
  ListOfSearch: any;
  addProjectSwitcher: boolean = false;
  id: string = '';
  userIdtoSearch: string = '';
  teamData: Teams = new Teams;
  usersData: UserEntity[] = [];
  usersSkillsData: UserInfo[] = [];
  accessUser: boolean =
    ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
    accessUserTL: boolean =
    TaskUpperRoles.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;

  constructor(
    private projectService: ProjectService,
    public ar: ActivatedRoute,
    private userInfoService: UserInfoService,
    private teamService: TeamService,
    private userService: UserService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private sanitizer: DomSanitizer
  ) {
    this.ar.params.subscribe((data) => {
      this.id = data['id'];
    });
    this.teamService.get(this.id).subscribe((res: Teams) => {
      this.teamData = res;
      this.getUsersById();
    });
  }
  // Get users that belong to the current team
  getUsersById() {
    this.teamData.usersId?.forEach((id: any) => {
      this.userInfoService.get(id).subscribe((res: UserInfo) => {
        this.usersSkillsData.push(res);
      });
      this.userService.get(id).subscribe((res: UserEntity) => {
        this.usersData.push(res);
      });
      this.fetchProfilePicture(id);
    });
  }
  // Remove project from this Team
  removeProject(projectIdToRemove: any) {
    console.log(this.teamData.id as string, projectIdToRemove);
    this.teamService
      .addOrRemoveProject(this.teamData.id as string, projectIdToRemove)
      .subscribe(() => {
        this.router
          .navigateByUrl('/', { skipLocationChange: true })
          .then(() => {
            this.router.navigate([
              '/Dashboard/Teams/Details',
              this.teamData.id,
            ]);
          });
        // Perform any necessary operations with the project
      });
  }
  // Function that exicute in two way
  // 1- Add user to the team
  // 2- Remove user from the team
  addOrRemoveUser(userId: string) {
    this.teamService
      .addOrRemoveUser(this.teamData.id as string, userId)
      .subscribe(() => {
       window.location.reload();
      });
  }

  // Add to Department Users /Skills
  //On Opning model get informations
  ModelAddSmothingToDepartment() {
    this.userService.getAllWithpage(1, 10, this.userIdtoSearch).subscribe(
      (res: any) => {
        this.ListOfSearch = res.userDtoList;
      },
      (err) => {
        this._snackBar.open(
          'We Found An Error During Getting Users Please refresh this page  !!!',
          '❌'
        );
      }
    );
  }

  // add project
  listProjects: Project[] = []; // List to store the available projects
  project = new FormControl(); // FormControl to track the selected projects

  fetchProjects() {
    this.addProjectSwitcher = !this.addProjectSwitcher;
    this.projectService.getAll().subscribe((res: Project[]) => {
      this.listProjects = res;
    });
  }
  //Get Selected Project from the Select tag to add it to list and save team's projects in database
  getSelectedProjects() {
    // Retrieve the selected projects
    const selectedProjects = this.project.value;
    // Retrieve the IDs of the selected projects
    selectedProjects.forEach((projectId: string) => {
      this.teamService
        .addOrRemoveProject(this.teamData.id as string, projectId)
        .subscribe(
          () => {
            this.router
              .navigateByUrl('/', { skipLocationChange: true })
              .then(() => {
                this.router.navigate([
                  '/Dashboard/Teams/Details',
                  this.teamData.id,
                ]);
              });
          },
          () => {
            this._snackBar.open(
              'We Found An Error During Adding Project Please refresh this page  !!!',
              '❌'
            );
          }
        );
    });
  }
  // Changing the status of the current team
  // InWorking Status
  // Not Available Status
  changeStatus() {
    if (this.teamData.status == 'inWorking') {
      this.teamData.status = 'Not Available';
    } else {
      this.teamData.status = 'inWorking';
    }
    this.teamService
      .Update(this.teamData.id as string, this.teamData)
      .subscribe(
        () => {
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then(() => {
              this.router.navigate([
                '/Dashboard/Teams/Details',
                this.teamData.id,
              ]);
            });
        },
        () => {
          this._snackBar.open(
            'We Found An Error During Updating Team Please refresh this page  !!!',
            '❌'
          );
        }
      );
  }
      // get Profile Picture
// Define a property to store the profile picture URLs
userProfilePictures: { [userId: string]: SafeUrl } = {};

// Call the function to fetch the profile picture for a user
fetchProfilePicture(userId: string): void {
  this.userService.getFile(userId).subscribe(
    (res: any) => {
      let objectURL = URL.createObjectURL(res);
      this.userProfilePictures[userId] = this.sanitizer.bypassSecurityTrustUrl(objectURL);
    },
    (error) => {
      this.userProfilePictures[userId] = LogoImgPath;
    }
  );
}
}
