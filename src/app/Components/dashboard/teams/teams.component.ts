import { Component } from '@angular/core';
import { DepartmentService } from '../../../Services/Soprahr/department.service';
import { TeamService } from 'src/app/Services/Teams/team.service';
import { Teams } from 'src/app/models/Teams';
import { ManagerRole } from '../../../../assets/roles.constants';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { HigherGrantedRoles } from 'src/assets/roles.constants';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css'],
})
export class TeamsComponent {
  Deps: any;
  DepIdChosen: any;
  Teams!: Teams[];
  newTeam = new Teams();

  accessUser: boolean =
    ManagerRole.indexOf(this.tokenStorage.getRole()) == -1 ? false : true;
  accessHigherGrantedUser: boolean =
    HigherGrantedRoles.indexOf(this.tokenStorage.getRole()) == -1
      ? false
      : true;
  constructor(
    private departmentService: DepartmentService,
    private teamService: TeamService,
    private tokenStorage: TokenStorageService,
    private userService: UserService,
    private matSnackBar: MatSnackBar,
    private router: Router
  ) {
    this.userService
      .get(this.tokenStorage.getUser() as string)
      .subscribe((res: UserEntity) => {
        this.DepIdChosen = res.departmentId;
      });
    if (this.accessHigherGrantedUser) {
      this.getDeps();
    } else {
      this.getTeamByUserId();
    }
  }
  onChangeDep() {
    this.getTeamsByDepId();
  }
  //Admin Functions
  // Get Departments from the first microservices to orgonize teams
  getDeps() {
    this.departmentService.getAll().subscribe(
      (res: any) => {
        this.DepIdChosen = res[0].id;
        this.Deps = res;
        this.getTeamsByDepId();
      },
      (error) => {
        this.matSnackBar.open(
          'Error while trying to get Departments please try to refresh page',
          '❌',
          {
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }
        );
      }
    );
  }
  // get team based on the the department choosen by admin to display
  getTeamsByDepId() {
    this.teamService.getTeamsByDepId(this.DepIdChosen).subscribe(
      (res: Teams[]) => {
        this.Teams = res;
      },
      (error) => {
        this.matSnackBar.open(
          'Error while trying to get your Teams please try to refresh page',
          '❌',
          {
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }
        );
      }
    );
  }
  //User Functions
  // get Teams Based on the authenticated user
  getTeamByUserId() {
    this.teamService
      .getTeamByUserId(this.tokenStorage.getUser() as string)
      .subscribe(
        (res: any) => {
          this.Teams = res;
        },
        (error) => {
          this.matSnackBar.open(
            'Error while trying to get your Teams please try to refresh page',
            '❌',
            {
              panelClass: ['red-snackbar'],
              horizontalPosition: 'right',
              verticalPosition: 'top',
            }
          );
        }
      );
  }
  // Function to delete a team based on it's id from database
  deleteTeam(id: any) {
    if(confirm('Are you sure you want to delete')){
    this.teamService.Delete(id).subscribe(
      (res: any) => {
        this.matSnackBar.open('Team Deleted Successfully', '❌', {
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
        this.router
          .navigateByUrl('/', { skipLocationChange: true })
          .then(() => {
            this.router.navigate(['/Dashboard/Teams']);
          });
      },
      (error) => {
        this.matSnackBar.open('Error while trying to delete team', '❌', {
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      }
    );
    }
  }
  // Function that Add new team to Database
  addTeam() {
    if(this.newTeam.name !=""){
      this.newTeam.departmentId = this.DepIdChosen;
      this.newTeam.status = 'Not Available';
      this.newTeam.usersId?.push(this.tokenStorage.getUser() as string);
      this.newTeam.id = null;
      this.teamService.Create(this.newTeam).subscribe(
        (res: any) => {
          this.matSnackBar.open('Team Added Successfully', '❌', {
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then(() => {
              this.router.navigate(['/Dashboard/Teams']);
            });
        },
        (error) => {
          this.matSnackBar.open('Error while trying to add a new team', '❌', {
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          });
        }
      );
    }else{
      this.matSnackBar.open(
        'Please enter a valid team name',
        '❌',
      )
    }
  }
}
