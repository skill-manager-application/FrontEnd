import { Component } from '@angular/core';
import { EntryService } from 'src/app/Services/AuthServices/entry.service';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import {
  HigherGrantedRoles,
  ManagerRole,
} from '../../../assets/roles.constants';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent {
  //Declaration
  //Path of logo img
  LogoImgPath = '../../assets/soprahr.png';
  // User Status
  isLoggedIn: boolean = false;
  //Dark Mode or Light Mode
  darkMode : boolean = false;
  userData: UserEntity = {
    userId: '',
    firstname: '',
    lastname: '',
    email: '',
    siteId: 0,
    siteName: '',
    departmentId: '',
    departmentDomain: '',
    roleId: '',
    role: new RoleEntity(0, '', ''),
  };
  page: string = "Home";
  imageProfile!: SafeUrl;
  accessHigherUser: boolean =
    HigherGrantedRoles.indexOf(this.tokenStorageService.getRole()) == -1
      ? false
      : true;
  accessManagerRole: boolean =
    ManagerRole.indexOf(this.tokenStorageService.getRole()) == -1
      ? false
      : true;
  constructor(
    private userService: UserService,
    private tokenStorageService: TokenStorageService,
    private EService: EntryService,
    private sanitizer: DomSanitizer,
    private MatSnackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.isLoggedIn != !this.tokenStorageService.getToken();
    this.refreshProfile();
  }
  //Method to the profile
  refreshProfile() {
    const userAuth = this.tokenStorageService.getUser();
    this.getImage(userAuth as string);
    this.userService.get(userAuth).subscribe((res: UserEntity) => {
      this.userData = res;
    },(error)=>{
      this.MatSnackBar.open(error.error.message,'❌',{
        duration: 3000
        })
    });
  }

  //Method to logout
  signOut() {
    this.EService.signOut();
  }
  status = false;
  addToggle() {
    this.status = !this.status;
  }
  // Get User Image
  getImage(userId: string) {
    this.userService.getFile(userId).subscribe(
      (res: any) => {
        let objectURL = URL.createObjectURL(res);
        this.imageProfile = this.sanitizer.bypassSecurityTrustUrl(objectURL);
      },
      () => {
        this.imageProfile =
          'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp';
      }
    );
  }

  modeChanges(){
    this.darkMode = !this.darkMode;
    console.log(this.darkMode);
  }
}
