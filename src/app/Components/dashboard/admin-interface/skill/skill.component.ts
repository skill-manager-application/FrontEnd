import { Component } from '@angular/core';
import {FormControl} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { CategoryService } from 'src/app/Services/Skill/category.service';
import { SkillService } from 'src/app/Services/Skill/skill.service';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.css']
})
export class SkillComponent {
  categoryData : any ;
  categoryChosen : any ;
  newCategory : string ="";
  constructor(private router : Router ,private categoryService : CategoryService,private skillService : SkillService,private MatSnackBar : MatSnackBar){}

  ngOnInit() {
    this.getCategoryData();
  }
  getCategoryData(){
    this.categoryService.getAll().subscribe((res : any)=>{
      this.categoryData = res;
      this.categoryChosen = res[0];
    })
  }

  changeCategory(event : any){
    this.categoryData.forEach((element:any) => {
      if(element.id == event.target.value){
        this.categoryChosen = element;
      }
    });
  }

  deleteSkill(id : any){
    if(confirm("are you sure")){
      this.skillService.Delete(id).subscribe((res : any)=>{
        window.location.reload();
      })
    }
  }
  addCategory(){
   if(this.newCategory != ''){
    this.categoryService.Create({name : this.newCategory}).subscribe((res : any)=>{
      window.location.reload();
    },(error)=>{
      this.MatSnackBar.open("Category already exists", 'Close', {
        duration: 1000,
        panelClass: ['red-snackbar'],
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
    })
   }else{
    this.MatSnackBar.open("Please don't leave fileds empty", 'Close', {
      duration: 1000,
      panelClass: ['red-snackbar'],
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
   }
  }
  deleteCategory(){
    if(confirm("are you sure")){
      this.categoryService.Delete(this.categoryChosen.id).subscribe((res : any)=>{
        window.location.reload();
        this.MatSnackBar.open(
          'Category deleted successfully',
          'Close',
          {
            duration: 600,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'right',
            verticalPosition: 'top',
          }
        )
      },(error)=>{
        this.MatSnackBar.open("Category already exists", 'Close', {
          duration: 1000,
          panelClass: ['red-snackbar'],
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });
      })
    }
  }
}
