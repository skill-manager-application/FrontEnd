import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipEditedEvent, MatChipInputEvent } from '@angular/material/chips';
import { CategoryService } from './../../../../../Services/Skill/category.service';
import { Skill } from 'src/app/models/skill';
import { QuestionService } from 'src/app/Services/Skill/question.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-edit-skill',
  templateUrl: './add-edit-skill.component.html',
  styleUrls: ['./add-edit-skill.component.css'],
})
export class AddEditSkillComponent {
  allQuestions: any;
  id!: any;
  mode: string = 'skill';
  categoryChosen?: string;
  skill: Skill = new Skill();
  nbrOptions: number[] = [];
  question: any = {
    questionText: '',
    explanation: '',
    options: [],
    skillId: '',
  };
  categories: any;
  constructor(
    public ar: ActivatedRoute,
    private skillService: SkillService,
    private categoryService: CategoryService,
    private router: Router,
    private questionService: QuestionService,
    private MatSnackBar : MatSnackBar
  ) {
    // Get the id from uri if exist
    this.ar.params.subscribe((data) => {
      this.id = data['id'];
    });
    if (this.id != null) {
      // Get the skill from id
      this.skillService.get(this.id).subscribe((data) => {
        this.skill = data;
        this.keywords = data.keywords;
      });
      // Get all question of this skill
      this.questionService.get(this.id).subscribe((res: any) => {
        this.allQuestions = res;
      });
    }
    // Get all categories in case that the user want to change the category of this skill
    this.categoryService.getAll().subscribe((res: any) => {
      this.categories = res;
    });
  }

  // Add new question to the skill quiz
  addNewOption(f: NgForm) {
    console.log(f.value['correct']);
    if ((f.value['text'] != null, f.value['correct'] != null)) {
      let newOption = { text: f.value['text'], correct: f.value['correct'] };
      this.question.options.push(newOption);
      f.controls['text'].reset();
      f.controls['correct'].reset();
    }
  }

  // Remove question from the skill quiz
  saveQuestion(f: NgForm) {
    f.value['question'] = '';
    this.updateQuestion();
  }
  //
  deleteItem(index: number) {
    this.question.options.splice(index, 1);
  }

  // Save the Question in dataBase
  onSubmitSkillForm() {
    this.skill.keywords = this.keywords;
    // Check if the the skill already exist then update it
    if(this.skill.name !="" && this.skill.description !="" && this.skill.level != 0){
      if (this.skill.id) {
        this.skillService
          .Update(this.skill.id, this.skill)
          .subscribe(() => {
            window.location.reload();
          });
      } else {
        // Create new skill
        this.skillService
          .CreateSkill(this.categoryChosen, this.skill)
          .subscribe((res: any) => {
            this.router.navigate(['/Dashboard/EditSkill/', res.id]);
          });
      }
    }else{
      this.MatSnackBar.open(
        'Please fill all the fields',
        'Close',
      )
    }
  }
  // Add Question to the list
  addQuestion() {
    this.mode = 'question';
    this.question = {
      questionText: '',
      explanation: '',
      options: [],
      skillId: this.id,
    };
  }

  // Open Question
  openQuestion(item: any) {
    this.mode = 'question';
    this.question = item;
  }

  //update question in the dataBase
  updateQuestion() {
    this.question.skillId = this.id;
    this.questionService.Update(null, this.question).subscribe(
      (res) => {
        this.allQuestions.push(this.question);
        this.MatSnackBar.open(
          'Question Updated Successfully',
          'Close',
          {
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          }
        )
      },
      (error) => {
        console.log(error);
      }
    );
  }
  // Delete Question from the dataBase
  deleteQuestion() {
    this.questionService.Delete(this.question.id).subscribe(
      (res) => {
        this.MatSnackBar.open(
          'Question Deleted Successfully',
          'Close',
        )
        window.location.reload();
      },
      (error) => {
        this.MatSnackBar.open(
          'Error',
          'Close',
        )
      }
    );
  }

  // Keywords for Skills
  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  keywords: any[] = [];

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.keywords.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(keyword: any): void {
    const index = this.keywords.indexOf(keyword);

    if (index >= 0) {
      this.keywords.splice(index, 1);
    }
  }

  edit(keyword: any, event: MatChipEditedEvent) {
    const value = event.value.trim();

    // Remove fruit if it no longer has a name
    if (!value) {
      this.remove(keyword);
      return;
    }

    // Edit existing fruit
    const index = this.keywords.indexOf(keyword);
    if (index >= 0) {
      this.keywords[index] = value;
    }
  }
}
