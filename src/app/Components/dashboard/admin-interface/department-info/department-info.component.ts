import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DepartmentService } from '../../../../Services/Soprahr/department.service';
import { MatDialog } from '@angular/material/dialog';
import { SiteService } from 'src/app/Services/Soprahr/site.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';
import { SkillService } from 'src/app/Services/Skill/skill.service';
import { UserInfoService } from 'src/app/Services/Skill/user-info.service';
import { UserService } from 'src/app/Services/Soprahr/user.service';
import { UserEntity } from 'src/app/models/SopraModels/UserEntity';
import { DepartmentEntity } from 'src/app/models/SopraModels/DepartmentEntity';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { HigherGrantedRoles } from '../../../../../assets/roles.constants';

@Component({
  selector: 'app-department-info',
  templateUrl: './department-info.component.html',
  styleUrls: ['./department-info.component.css'],
})
export class DepartmentInfoComponent {
  //Department Values
  departmentInfo!: DepartmentEntity;
  dataDiagram: any = {
    skillName: [],
    nbrSkills: ([] = [0]),
    avgLevel: [],
    SkillLevel: [],
  };
  departments: any = [];
  usersList: UserEntity[] = [];
  id: number = 0;

  // Table users Values
  displayedColumns: string[] = ['name', 'email', 'role'];
  length: number = 0;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25];
  panelOpenState = true;

  // Charts Values
  boxed: string = '';
  site: any = {};
  option: any;
  optionDynamic: any;
  dataSource!: any;
  rolesData: any[] = [];
  skillsData: any[] = [];

  // user input To Search
  userIdtoSearch : string ="";
  // true if the user select Add user and false if the user select to add a skill to this department
  typeSearch: boolean = true;
  // list of users with
  ListOfSearch: UserEntity[] = [];

  accessUser : boolean = HigherGrantedRoles.indexOf(this.TokenStorageService.getRole()) == -1 ? false : true;

  constructor(
    private siteService: SiteService,
    public ar: ActivatedRoute,
    private departmentService: DepartmentService,
    private router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private skillService: SkillService,
    private userInfoService: UserInfoService,
    private userService: UserService,
    private TokenStorageService : TokenStorageService
  ) {
    this.ar.params.subscribe((data) => {
      this.id = data['id'];
      this.getUsers();
      this.getDepartment();
    });
  }
  //Get Department Values
  getDepartment() {
    this.departmentService.get(this.id)
    .subscribe(
      (data: DepartmentEntity) => {
      this.departmentInfo = data;
      this.departmentsInSite(this.departmentInfo.siteId);
      },
      (err) => {
        this._snackBar.open(
          'We Found An Error During Getting Department Please refresh this page  !!!',
          '❌'
        );
      }
    );
  }
  // Get All Department in this Department Site
  departmentsInSite(siteId: any) {
    this.siteService.get(siteId).subscribe(
      (res: any) => {
        this.site = res;
        this.departments = res.departments;
      },
      (errr) => {
        this._snackBar.open(
          'We Found An Error During Getting Departments Please refresh this page  !!!',
          '❌'
        );
      }
    );
    // Reset dropdown selection to default value
    this.departmentInfo.departmentMotherId = 0;
  }
  // Get all Users in this Department
  getUsers(
    page: number = this.pageIndex,
    limit: number = this.pageSize,
    departmet: number = this.id
  ) {
    this.userService
      .getUsersByDepartmentId(departmet, page + 1, limit)
      .subscribe(
        (res: any) => {
          this.usersList = res;
          this.usersList = res.userDtoList;
          this.length = res.totalPages * this.pageSize;
          this.getUserRoleForChart();
          this.getSkills();
          this.dataSource = new MatTableDataSource<UserEntity[]>(
            res.userDtoList
          );
        },
        (err) => {
          this._snackBar.open(
            'We Found An Error During Getting Users Please refresh this page  !!!',
            '❌'
          );
        }
      );
  }

  // Get all Skills in this Department
  getSkills() {
    this.skillsData = [];
    this.usersList.forEach(
      (user: UserEntity) => {
      this.userInfoService.get(user.userId).subscribe((res: any) => {
        if (res != null) {
          this.skillsData.push(res);
        }
      });
    },
    () => {
      this._snackBar.open(
        'We Found An Error During Getting Skills Please refresh this page  !!!',
        '❌'
      );
    });
    setTimeout(() => {
      this.createData();
    }, 50);
  }

  // Create Data For Skills Chart
  createData() {
    this.dataDiagram = {
      skillName: [],
      nbrSkills: ([] = [0]),
      avgLevel: [],
      SkillLevel: [],
    };
    this.skillsData.forEach((user: any) => {
      user.skills.forEach((skill: any) => {
        if (
          !this.dataDiagram.skillName.some(
            (element: any) => element == skill?.skillName
          )
        ) {
          this.dataDiagram.skillName.push(skill?.skillName);
          this.dataDiagram.nbrSkills.push(
            this.dataDiagram.nbrSkills[this.dataDiagram.nbrSkills.length - 1] +
              1
          );
          this.dataDiagram.avgLevel.push(skill?.avgLevel);
          this.skillService.get(skill?.skillId).subscribe((res: any) => {
            this.dataDiagram.SkillLevel.push(Number(res.level));
          });
        } else {
          this.dataDiagram.avgLevel[
            this.dataDiagram.skillName.indexOf(skill?.skillName)
          ] =
            (this.dataDiagram.avgLevel[
              this.dataDiagram.skillName.indexOf(skill?.skillName)
            ] +
              skill?.avgLevel) /
            2;
        }
      });
    });
    this.createChart();
  }
  // Create Chart Skills
  createChart() {
    setTimeout(() => {
      this.dataDiagram.nbrSkills.pop();
      this.optionDynamic = {
        title: {
          text: 'Skills',
        },
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              backgroundColor: '#db1846',
            },
          },
        },
        legend: {},
        toolbox: {
          show: true,
          feature: {
            dataView: { readOnly: false },
            restore: {},
            saveAsImage: {},
          },
        },
        dataZoom: {
          show: false,
          start: 0,
          end: 100,
        },
        xAxis: [
          {
            type: 'category',
            boundaryGap: true,
            data: this.dataDiagram.skillName,
          },
          {
            type: 'category',
            boundaryGap: true,
            data: this.dataDiagram.nbrSkills,
          },
        ],
        yAxis: [
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2],
          },
          {
            type: 'value',
            scale: true,
            name: 'Level',
            max: 10,
            min: 0,
            boundaryGap: [0.2, 0.2],
          },
        ],
        series: [
          {
            name: 'Avg Level for collaborateur',
            type: 'bar',
            xAxisIndex: 1,
            yAxisIndex: 1,
            data: this.dataDiagram.avgLevel,
          },
          {
            name: 'Skill Required level',
            type: 'line',
            data: this.dataDiagram.SkillLevel,
          },
        ],
      };
    }, 500);
  }
  //Create chart For Roles
  getUserRoleForChart() {
    this.rolesData = [];
    this.usersList.forEach((user: any) => {
      if (this.rolesData.length == 0) {
        this.rolesData.push({ name: user.role.roleName, value: 1 });
      } else {
        this.rolesData.forEach((role) => {
          if (role.name === user.role.roleName) {
            role.value++;
          }
        });
        const roleExists = this.rolesData.some((role) => {
          return role.name === user.role.roleName;
        });
        if (!roleExists) {
          this.rolesData.push({ name: user.role.roleName, value: 1 });
        }
      }
    });
    this.option = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      legend: {
        top: 'bottom',
      },

      series: [
        {
          name: 'Role:User In the Department',
          type: 'pie',
          radius: ['40%', '70%'],
          avoidLabelOverlap: false,
          itemStyle: {
            borderRadius: 10,
            borderColor: '#fff',
            borderWidth: 2,
          },
          label: {
            show: false,
            position: 'center',
          },
          emphasis: {
            label: {
              show: true,
              fontSize: 40,
              fontWeight: 'bold',
            },
          },
          labelLine: {
            show: false,
          },
          data: this.rolesData,
        },
      ],
    };
  }

// Model Edit Department
  // Delete Department in dataBase
  deleteDeptrment() {
    if (
      confirm('Are you sure you want to delete' + this.departmentInfo.domain)
    ) {
      this.departmentService.Delete(this.id).subscribe(
        (data) => {
        this.router.navigate(['/Dashboard/Admin/Site']);
        },
        (err) => {
          this._snackBar.open(
            'We Found An Error During Deleting Department Please refresh this page  !!!',
            '❌'
          );
        }
      );
    }
  }
  // Edit Department in DataBase
  editDepartment() {
    if(this.departmentInfo.domain !=""){
      this.departmentService
      .Update(this.departmentInfo.id, {
        siteId: this.departmentInfo.siteId,
        domain: this.departmentInfo.domain,
        departmentType: this.departmentInfo.departmentType,
        departmentMotherId: this.departmentInfo.departmentMotherId,
      })
      .subscribe(
        (res) => {
          this._snackBar.open('Department Edited Successfully !!!', '✔️');
          window.location.reload();
        },
        (errr) => {
          this._snackBar.open(
            'We Found An Error During Creating New Department Please refresh this page  !!!',
            '❌'
          );
        }
      );
    }else{
      this._snackBar.open(
        'Please Fill All Fields !!!',
        '❌'
      )
    }
  }
// Add to Department Users /Skills
  //On Opning model get informations
  ModelAddSmothingToDepartment() {
    if (this.typeSearch) {
      this.userService
        .getAllWithpage(1, 3, this.userIdtoSearch)
        .subscribe((res: any) => {
          this.ListOfSearch = res.userDtoList;
        },
        (err) => {
          this._snackBar.open(
            'We Found An Error During Getting Users Please refresh this page  !!!',
            '❌'
          );
        }
        );
    } else {
      //cod to add skill
    }
  }
  //Add User/Skill to Department
  AddtoDepartment(user: UserEntity) {
    user.departmentId = this.id.toString();
    user.roleId = user.role.idRole.toString();
    if (this.typeSearch) {
      this.userService.Update(user.userId, user).subscribe(
        () => {
          this._snackBar.open('User Added Successfully !!!', '✔️');
          window.location.reload();
        },
        () => {
          this._snackBar.open(
            'We Found An Error During Adding User Please refresh this page  !!!',
            '❌'
          );
        }
      );
    }
  }
// Actions
  // Pagination users
  handlePageEvent(e: PageEvent) {
    this.pageSize = e.pageSize;
    this.pageIndex = e.pageIndex;
    this.getUsers(this.pageIndex, this.pageSize);
  }
  // box to open Actions
  toggleBox() {
    this.boxed = this.boxed == '' ? 'open' : '';
  }
}
