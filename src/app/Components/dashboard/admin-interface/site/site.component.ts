import { Component } from '@angular/core';
import { SiteService } from 'src/app/Services/Soprahr/site.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DepartmentService } from '../../../../Services/Soprahr/department.service';
import { Router } from '@angular/router';

import { SiteEntity } from 'src/app/models/SopraModels/SiteEntity';
import { DepartmentEntity } from 'src/app/models/SopraModels/DepartmentEntity';
import { TokenStorageService } from 'src/app/Services/AuthServices/token-storage.service';
import { ManagerRole } from '../../../../../assets/roles.constants';
import { HigherGrantedRoles } from 'src/assets/roles.constants';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css'],
})
export class SiteComponent {
  constructor(
    private siteService: SiteService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private departmentService: DepartmentService,
    private TokenStorageService : TokenStorageService
  ) {
    this.getSitesAndDepartments();
  }

  higherGrantedRoles : boolean = HigherGrantedRoles.indexOf(this.TokenStorageService.getRole()) == -1 ? false : true;

  accessUser : boolean = ManagerRole.indexOf(this.TokenStorageService.getRole()) == -1 ? false : true;
  option: any;
  chartInstance: any;
  data: any = {
    name: 'Sopra Steria',
    type: 'Company Origin',
    lineStyle: {
      width: 40,
    },
    value: '',
    children: [],
  };


  departments: any = [];
  sites: SiteEntity[] = [];
  DepartmentBySite: DepartmentEntity[] = [];
  newSite: SiteEntity = {
    id: 0,
    name: '',
    local: '',
    country: '',
    departments: [],
  };
  newDepartment: DepartmentEntity = {
    id: 0,
    domain: '',
    departmentMotherId: 0,
    departmentType: '',
    siteId: 0,
  };
  ngOnInit() {
    setTimeout(() => {
      this.getData();
      this.CreateChart();
    }, 800);
  }
  // Events On the Chart
  // OnClick On chart
  onChartEvent($event: any) {
    setTimeout(() => {
      const result = this.chartInstance.getModel().getSeries();
    }, 50);

    const local = $event.data.local.split(',').join(' > ');
    if ($event.data.value && !$event.data.children) {
      this.router.navigate(['/Dashboard/Department/', $event.data.value]);
    }
  }
  // onRight Click to Open Department Details
  onRightClick($event: any) {
    if ($event.data.type != 'site') {
      this.router.navigate(['/Dashboard/Department/', $event.data.value]);
    }
  }
  // On Chart Inisialization
  onChartInit(e: any) {
    this.chartInstance = e;
  }
  // Create Chart Models And Forms
  CreateChart() {
    this.option = {
      tooltip: {
        trigger: 'item',
        triggerOn: 'mousemove',
        position: (
          point: any[],
          params: any,
          dom: any,
          rect: any,
          size: { viewSize: any; contentSize: any }
        ) => {
          const sv = size.viewSize;
          const sc = size.contentSize;
          const posX =
            point[0] < sv[0] / 2
              ? point[0] + sc[0] / 3.6
              : point[0] - sc[0] * 1.2;
          const posY = point[1] > sv[1] ? point[1] - sc[1] / 2 : point[1];
          return [posX, posY];
        },

        formatter: (v: any) => {
          const template = `

            <div style="min-width:8rem; font-size:0.6rem">
              <div style="font-weight:bold">[Type]</div>
              <div>${v.data.type}</div><br>
              <div style="font-weight:bold">[Name]</div>
              <div>${v.name}</div><br>
              <div style="font-weight:bold">[Local]</div>
              <div>${v.data.local}</div><br>
              <div style="font-weight:bold">[ID]</div>
              <div style="font-size: 1rem; color:#00BCD4">${
                +v.value == 0 ? 'No ID' : +v.value
              }</div><br>
            </div>
          `;
          const template2 = `
            <div style="min-width:8rem; font-size:0.6rem">
              <div style="font-weight:bold">[Type]</div>
              <div>${v.data.type}</div><br>
              <div style="font-weight:bold">[Domain]</div>
              <div>${v.name}</div><br>
              <div style="font-weight:bold">[ID]</div>
              <div style="font-size: 1rem; color:#00BCD4">${+v.value}</div><br>
            </div>
          `;
          return v.data.type == 'site' ? template : template2;
        },
      },

      series: [
        {
          type: 'tree',
          data: [this.data],
          top: '0%',
          left: '10%',
          bottom: '20%',
          right: '26%',

          symbolSize: 0,

          initialTreeDepth: 1,

          label: {
            color: '#fff',
            formatter: (v: any) => {
              const nodeStyles = [
                `{valueBg| ${+v.value || 0}}`,
                `{nodeBg| ${
                  v.name.length > 7 ? v.name.substring(0, 7) + '...' : v.name
                }}`,
              ].join(' ');
              const leafStyles = [
                `{valueBg| ${+v.value || 0}}`,
                `{leafBg| ${
                  v.name.length > 7 ? v.name.substring(0, 7) + '...' : v.name
                }}`,
              ].join(' ');
              const styles = !!v.data.children ? nodeStyles : leafStyles;
              return styles;
            },
            rich: {
              valueBg: {
                backgroundColor: 'rgba(0,23,11,0.3)',
                align: 'center',
                verticalAlign: 'middle',
                borderRadius: 4,
                width: 26,
                padding: [4, 8],
                shadowBlur: 12,
              },
              nodeBg: {
                backgroundColor: '#3C91E6',
                align: 'center',
                width: 60,
                padding: 12,
                borderRadius: 12,
                shadowBlur: 12,
                shadowOffsetX: 2,
                shadowOffsetY: 2,
                shadowColor: '#aaa',
              },
              leafBg: {
                backgroundColor: 'rgb(255, 82, 82)',
                align: 'center',
                width: 60,
                padding: 12,
                borderRadius: 12,
                shadowBlur: 12,
                shadowOffsetX: 2,
                shadowOffsetY: 2,
                shadowColor: '#aaa',
              },
            },
          },

          leaves: {
            label: {
              position: 'right',
              verticalAlign: 'middle',
              align: 'left',
            },
          },
        },
      ],
    };
  }
  // Create Data For chart
  getData() {
    for (let i = 0; i < this.sites.length; i++) {
      const site = {
        name: this.sites[i].name,
        local: this.sites[i].local,
        type: 'site',
        lineStyle: {
          width: 30,
        },
        value: this.sites[i].id,
        children: [],
      };
      this.data.children.push(site);
      this.DepartmentBySite = this.sites[i].departments;
      for (let index = 0; index < this.DepartmentBySite.length; index++) {
        if (this.DepartmentBySite[index].departmentMotherId === 0) {
          const department: any = {
            local: this.DepartmentBySite[index].domain,
            name: this.DepartmentBySite[index].domain,
            type: this.DepartmentBySite[index].departmentType,
            lineStyle: {
              width: 20,
            },
            value: this.DepartmentBySite[index].id,
          };
          if (
            this.organizeData(
              this.DepartmentBySite,
              this.DepartmentBySite[index].id
            ).length > 0
          ) {
            const children: any[] = [];
            department.children = children;
            this.organizeData(
              this.DepartmentBySite,
              this.DepartmentBySite[index].id
            ).forEach((element) => {
              department.children.push(element);
            });
            this.data.children[i].children.push(department);
          } else {
            this.data.children[i].children.push(department);
            continue;
          }
        }
      }
    }
  }
  // Function to Sort Departments
  organizeData(departments: any[], parentId: number): any[] {
    return departments
      .filter((dept) => dept.departmentMotherId === parentId)
      .map((dept) => {
        return this.organizeData(departments, dept.id).length > 0
          ? {
              name: dept.domain,
              local: dept.domain,
              type: dept.departmentType,
              lineStyle: {
                width: 10,
              },
              value: dept.id,
              children: this.organizeData(departments, dept.id),
            }
          : {
              name: dept.domain,
              local: dept.domain,
              type: dept.departmentType,
              lineStyle: {
                width: 10,
              },
              value: dept.id,
            };
      });
  }
  // Create New Site in DataBase
  addNewSite() {
if(  this.newSite.name != ''&& this.newSite.local != '' && this.newSite.country!=  ''){
  this.siteService.Create(this.newSite).subscribe(
    (res: SiteEntity) => {
      const assiningDepartment: DepartmentEntity = {
        id: 0,
        domain: 'Assining',
        departmentMotherId: 0,
        departmentType: 'CS',
        siteId: res.id,
      };
      this.addNewDepartment(assiningDepartment);
      const directionDepartment: DepartmentEntity = {
        id: 0,
        domain: 'Direction',
        departmentMotherId: 0,
        departmentType: 'Direction',
        siteId: res.id,
      };
      this.addNewDepartment(directionDepartment);
      this._snackBar.open('Site Created Successfully !!!', '✔️');
      window.location.reload();
    },
    () => {
      this._snackBar.open(
        'We Found An Error During Creating New Site Please try again  !!!',
        '❌'
      );
    }
  );
}else{
  this._snackBar.open(
    "Please don't leave fileds empty",
    '❌'
  );
}
  }
  // Create New Department in DataBase
  addNewDepartment(department: DepartmentEntity = this.newDepartment) {
   if(department.domain != '' && department.siteId != 0) {
    this.departmentService.Create(department).subscribe(
      (res) => {
        this._snackBar.open('Department Created Successfully !!!', '✔️');
        window.location.reload();
      },
      (errr) => {
        this._snackBar.open(
          'We Found An Error During Creating New Department Please refresh this page  !!!',
          '❌'
        );
      }
    );
   }else{
    this._snackBar.open(
      "Please don't leave fileds empty",
      '❌'
    );
   }
  }
  // Get Departments By Site Id For Model To Create New Department
  getDepartmentsBySiteId(siteId: any) {
    this.departments = this.sites.filter((el) => el.id == siteId);
    this.departments = this.departments[0].departments;
    // Reset dropdown selection to default value
    this.newDepartment.departmentMotherId = 0;
  }
  // Get All Nessecary Data From DataBase
  getSitesAndDepartments() {
    this.siteService.getAll().subscribe(
      (res: SiteEntity[]) => {
        this.sites = res;
        res.forEach((el: SiteEntity) => {
          this.departmentService.getDepartmentBySite(el.id).subscribe(
            (res: DepartmentEntity[]) => {
              el.departments = res;
            },
            () => {
              this._snackBar.open(
                'We Found An Error During Getting Departments in this Site' +
                  el.id +
                  'Please refresh this page  !!!',
                '❌'
              );
            }
          );
        });
      },
      () => {
        this._snackBar.open(
          'We Found An Error During Getting All Sites Please refresh this page  !!!',
          '❌'
        );
      }
    );
  }
}
