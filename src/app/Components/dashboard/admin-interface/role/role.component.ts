import { Component, ViewChild } from '@angular/core';
import { RoleService } from '../../../../Services/Soprahr/role.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { RoleEntity } from 'src/app/models/SopraModels/RoleEntity';

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}
@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent {
  @ViewChild('tree') tree :any;

  private _transformer = (node: any, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  };
  treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level,
    node => node.expandable,
  );
  treeFlattener = new MatTreeFlattener(
    this._transformer,
    node => node.level,
    node => node.expandable,
    node => node.children,
  );
  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  data : any=[]
  option: any;
  rolesListSorted :RoleEntity[]=[];
  colorArray :any = {
    red: {
      children: [
        { color: "#ff6961" },
        { color: "#cd5c5c" },
        { color: "#dc143c" },
        { color: "#8b0000" }
      ]
    },
    orange: {
      children: [
        { color: "#ff8c00" },
        { color: "#ffa500" },
        { color: "#ff7f50" },
        { color: "#ff6347" }
      ]
    },
    yellow: {
      children: [
        { color: "#ffff00" },
        { color: "#ffeb3b" },
        { color: "#f0e68c" },
        { color: "#ffd700" }
      ]
    },
    green: {
      children: [
        { color: "#2ecc71" },
        { color: "#00ff00" },
        { color: "#32cd32" },
        { color: "#008000" }
      ]
    },
    blue: {
      children: [
        { color: "#1e90ff" },
        { color: "#87ceeb" },
        { color: "#00bfff" },
        { color: "#0000ff" }
      ]
    }
  };
  colorName !: any ;
  color!:any;

  changedRole : RoleEntity = new RoleEntity(0,"","");
  jobTitle : string = "";
  roleName : string = "";

  newRole : RoleEntity = {
    idRole: 0,
    roleName: '',
    job_title: ''
  };

  constructor(private roleService : RoleService,private _MatSnackBar : MatSnackBar) {}
 ngOnInit() {
    this.sortedRolesData()
    setTimeout(() => {
     this.eChartSetOption(this.data);
     this.dataSource.data = this.data;
    }, 500);

 }
  getRoles() {
    this.roleService.getAll().subscribe((res:RoleEntity[])=> {
      this.rolesListSorted = res;
    },
    (error)=>{
      this._MatSnackBar.open("We Found An Error During Creating New Site Please refresh this page  !!!" +error.message, '❌')
    });
  }

  sortedRolesData() {
    this.getRoles();
    setTimeout(() => {
      const groupedRoles :any = {};
      const colors = this.colorArray
      for (const role of this.rolesListSorted) {
        this.colorName = Object.keys(colors)[Math.floor(Math.random() * Object.keys(colors).length)];
        if (!groupedRoles[role.roleName]) {
            groupedRoles[role.roleName] = {
            name: role.roleName,
            itemStyle: {
              color: this.colorName
            },
            children: []
          };
        }
        this.color = this.colorArray[ groupedRoles[role.roleName].itemStyle.color].children[Math.floor(Math.random() * this.colorArray[this.colorName].children.length)].color;
        const jobTitle = {
          name: role.job_title,
          itemStyle: {
            color: this.color
          },
          value: 1
        };
        groupedRoles[role.roleName].children.push(jobTitle);
      }
      this.data = Object.values(groupedRoles);
    }, 500);
  }
  /** eChart setting */
  eChartSetOption(data :any) {
    this.option = {
      series: {
        type: 'sunburst',

        data: data,
        radius: [0, '80%'],
        sort: undefined,

        emphasis: {
          focus: 'ancestor'
        },

        levels: [
          {},
          {
            r0: '15%',
            r: '35%',
            itemStyle: {
              borderWidth: 2
            },
            label: {
              rotate: 'tangential'
            }
          },
          {
            r0: '35%',
            r: '70%',
            label: {
              align: 'right'
            }
          },
          {
            r0: '70%',
            r: '72%',
            label: {
              position: 'outside',
              padding: 3,
              silent: false
            },
            itemStyle: {
              borderWidth: 3
            }
          }
        ]
      }
    };
  }

modifier(name : string){
  console.log(name);
  if(this.jobTitle == ''){
    this.roleName = name;
    this.jobTitle = name;
  }else{
    this.rolesListSorted.forEach(element => {
      element.job_title == name ? this.changedRole = element : 0;
    });
    this.changedRole.job_title = this.jobTitle;
    console.log(this.changedRole);
    this.roleService.Update(this.changedRole.idRole,this.changedRole).subscribe((res)=>{
      this.ngOnInit();
      this.roleName = "";
      this.jobTitle = "";
    })
  }
}
  addJobTitle(name : string){
      this.jobTitle = ""
      this.roleName = name;
  }

  addJobToDatabase(roleName : string, jobTitle : string,id : number = 0){
    const role : RoleEntity = {
      idRole: 0,
      roleName: roleName,
      job_title: jobTitle
    }
    this.roleService.Create(role).subscribe((res : RoleEntity)=>{
      this._MatSnackBar.open("Job Title Added Successfully", '✔️')
      this.ngOnInit();
      this.roleName = "";
      this.jobTitle = "";
    },
    (error)=>{
      this._MatSnackBar.open("We Found An Error During Creating New Role Please refresh this page  !!!" +error.message, '❌')
    });
  }

  deleteJobTitle(name : string){
    this.rolesListSorted.forEach(element => {
      element.job_title == name ? this.changedRole = element : 0;
    });
    if(this.changedRole.idRole != 0){
      this.roleService.Delete(this.changedRole.idRole).subscribe((res)=>{
        this._MatSnackBar.open("Role Deleted Successfully", '✔️');
        this.ngOnInit();
      },
      (error)=>{
        this._MatSnackBar.open("Role can't be Deleted Because some users May still have it please try to empty this Role from users",'❌')
      });
    }else{
      this.roleService.DeleteAll(name).subscribe((res : any)=>{
        this._MatSnackBar.open("Role Deleted Successfully",'✔️');
        this.ngOnInit();
      },
      (error)=>{
        this._MatSnackBar.open("Role can't be Deleted Because some users May still have it please try to empty this Role from users",'❌')
      });

    }
  }

  addNewRole(){
    if(this.newRole.job_title="") this.newRole.job_title = "No JobTitle"
    this.roleService.Create(this.newRole).subscribe((res : RoleEntity)=>{
      this._MatSnackBar.open("Role Added Successfully", '✔️')
      this.ngOnInit();
    },
    (error)=>{
      this._MatSnackBar.open("We Found An Error During Creating New Role Please refresh this page  !!!" +error.message, '❌')
    });
  }
}

