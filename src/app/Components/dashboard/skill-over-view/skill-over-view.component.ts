import { Component } from '@angular/core';
import { CategoryService } from 'src/app/Services/Skill/category.service';
import { Cateogry } from 'src/app/models/Category';

@Component({
  selector: 'app-skill-over-view',
  templateUrl: './skill-over-view.component.html',
  styleUrls: ['./skill-over-view.component.css']
})
export class SkillOverViewComponent {
  categories : Cateogry[]=[];
  choosenCategory : Cateogry = new Cateogry();
  idCategory : string ="";
  constructor(
    private categoryService : CategoryService
  ) {this.getCategories() }
getCategories(){
  this.categoryService.getAll().subscribe((res :Cateogry[])=>{
    this.categories = res;
    this.choosenCategory = res[0];
  })
}

  onChangeCategory(){
    this.categories.forEach((el:Cateogry)=>{
      if(el.id == this.idCategory){
        this.choosenCategory= el;
      }
    })
  }
}
