import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillOverViewComponent } from './skill-over-view.component';

describe('SkillOverViewComponent', () => {
  let component: SkillOverViewComponent;
  let fixture: ComponentFixture<SkillOverViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillOverViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SkillOverViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
