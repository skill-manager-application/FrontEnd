import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
const  APIUrlQuestion ="http://localhost:8081/questions";

@Injectable({
  providedIn: 'root'
})
export class QuestionService  extends DataService{

  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlQuestion,http);}
}
