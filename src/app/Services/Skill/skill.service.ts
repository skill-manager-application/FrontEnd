import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
const  APIUrlSkill ="http://localhost:8081/skills";

@Injectable({
  providedIn: 'root'
})
export class SkillService extends DataService{
  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlSkill,http);}

  //get with id
  getSkill(id: any): Observable<any> {
    return this.https.get(`${APIUrlSkill}/${id}`);
  }

  CreateSkill(id : any,data : any){
    return this.https.post(`${APIUrlSkill}/${id}`,data);
  }

  getSkillsByCatogry(id : any){
    return this.https.get(`${APIUrlSkill}/Category/${id}`);
  }

}
