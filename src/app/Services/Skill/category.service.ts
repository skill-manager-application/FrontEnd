import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
const  APIUrlCategory ="http://localhost:8081/category";

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends DataService{

  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlCategory,http);}

}
