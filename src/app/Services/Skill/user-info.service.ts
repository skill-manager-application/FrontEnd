import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';
const  APIUrlUser ="http://localhost:8081/userInfo";
@Injectable({
  providedIn: 'root'
})
export class UserInfoService extends DataService{

  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlUser,http);}

    //get users superviser assessment
    getUsersBySuperViserId(id: any): Observable<any> {
      return this.https.get(`${APIUrlUser}/superViser/${id}`);
    }


    getCategoriesByUser(userId : any){
      console.log(`${APIUrlUser}/category/${userId}`)
      return this.https.get(`${APIUrlUser}/category/${userId}`);
    }

    assignppl(users : any [], skillId : string){
      return this.https.post(`${APIUrlUser}/addSkill/${skillId}`,users);
    }


    getAllById(users :any){
      return this.https.get(`${APIUrlUser}/userById`,users);
    }


  userAllDetails(id :any){
    //return this.https.get(`${APIUrlUser}/userById`,users);
  }


  putUserAferSelfAssessment(skillId : string , userInfo : any){
    return this.https.put(`${APIUrlUser}/passAssessment/${skillId}`,userInfo);
  }
  putUserAfterSuperviserAssessment(userInfo : any){
    return this.https.put(`${APIUrlUser}/superviserAssessment`,userInfo);
  }

  getUsersBySkillId(skillId : string){
    return this.https.get(`${APIUrlUser}/skill/${skillId}`);
  }

  addSkillToListUsers(skillId : string,usersId : string[]){
    return this.https.post(`${APIUrlUser}/addSkill/${skillId}`,usersId);
  }
}
