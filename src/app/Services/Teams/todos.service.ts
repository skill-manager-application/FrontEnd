import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { checkList } from 'src/app/models/Task';
const  APIUrlTodos ="http://localhost:8082/Todos";

@Injectable({
  providedIn: 'root'
})
export class TodosService  extends DataService{
  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlTodos,http);}

  getTodosByUserId(userId : String): Observable<any>{
    return this.https.get(APIUrlTodos+"/user/"+userId);
  }
  getTodosByProjectIdAndUserId(userId : string , projectId :string): Observable<any>{
    return this.https.get(APIUrlTodos+"/user/"+projectId+"/"+userId);
  }
  getCheckListsByUserId(userId: string): Observable<any[]>{
    return this.https.get<any[]>(`${APIUrlTodos}/checklists/${userId}`);
  }
}
