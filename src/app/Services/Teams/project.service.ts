import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
const  APIUrlProjects ="http://localhost:8082/Project";

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends DataService{
  constructor(http:HttpClient,private https:HttpClient){   super(APIUrlProjects,http);}

  CreateProject(idteam : any,project : any){
    return this.https.post(APIUrlProjects+"/"+idteam,project);
  }

  addNewFloder(idProject :any,folder :any){
    return this.https.patch(APIUrlProjects+"/addfolder/"+idProject,folder);
  }

  updateFolder(idProject :any,folder :any){
    return this.https.patch(APIUrlProjects+"/folder/"+idProject,folder);
  }
  getProejctByUserId(userId : string){
    return this.https.get(APIUrlProjects+"/user/"+userId);
  }
  getProejctBySuperviserId(userId : string){
    return this.https.get(APIUrlProjects+"/superviser/"+userId);
  }
}
