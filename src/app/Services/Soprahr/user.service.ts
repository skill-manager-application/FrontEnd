import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { Observable, catchError, throwError } from 'rxjs';
import { UserEntity } from '../../models/SopraModels/UserEntity';

const  APIUrlUser ="http://localhost:8080/api/users";
@Injectable({
  providedIn: 'root'
})
export class UserService  extends DataService{
  constructor(http:HttpClient,private httpPrivate : HttpClient){
    super(APIUrlUser,http);
  }

    getFile(userId: string): Observable<any> {
      return this.httpPrivate.get(`${APIUrlUser}/downloadFile/${userId}`, { responseType: 'blob' }).pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 500) {
            // Handle the specific error code
            console.log('File not found');
          } else {
            // Handle other error codes or general error
            console.error('An error occurred:', error.message);
          }
          return throwError('Something went wrong. Please try again later.');
        })
      );
    }
    // upload Image Profile For user
    uploadImage(id : string,uploadedImage : any){
      return this.httpPrivate.post(`${APIUrlUser}/uploadFile/${id}`,uploadedImage);
    }
    // Get Users By Department Id
    getUsersByDepartmentId(departmentId : number,page: number = 1, limit: number = 5){
      return this.httpPrivate.get<UserEntity[]>(`${APIUrlUser}/department/${departmentId}`+"?page="+page+"&limit="+limit);

    }
    // Get Users By Department Id
    getUsersByDepartmentIdUserResponse(departmentId : any){
      return this.httpPrivate.get<UserEntity[]>(`${APIUrlUser}/departmentUsers/${departmentId}`);
    }

    getUsersByRoleId(id : string){
      return this.httpPrivate.get<UserEntity[]>(`${APIUrlUser}/role/${id}`);
    }
    getusers(){
      return this.httpPrivate.get<UserEntity[]>(`${APIUrlUser}/users`);
    }

}

