import { Injectable } from '@angular/core';
import { DataService } from '../data.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RoleEntity } from '../../models/SopraModels/RoleEntity';
const  APIUrlRole ="http://localhost:8080/api/roles";

@Injectable({
  providedIn: 'root'
})
export class RoleService extends DataService{
  constructor(http:HttpClient,private https : HttpClient){
    super(APIUrlRole,http);
  }

    //Delete Method
    DeleteAll(data :string): Observable<RoleEntity> {
      return this.https.delete<RoleEntity>(`${APIUrlRole}/deleteAll/${data}`);
    }

}
