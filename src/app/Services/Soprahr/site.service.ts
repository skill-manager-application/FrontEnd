import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
const  APIUrlSite ="http://localhost:8080/api/sites";

@Injectable({
  providedIn: 'root'
})
export class SiteService  extends DataService{

  constructor(http:HttpClient){
    super(APIUrlSite,http);
  }
}
