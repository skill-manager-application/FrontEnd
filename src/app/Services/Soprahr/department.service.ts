import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
const  APIUrlDepartment ="http://localhost:8080/api/departments";
const  APIUrlDepSite ="http://localhost:8080/api/departments/site";

@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends DataService{

  constructor(http:HttpClient,private httpC:HttpClient){
    super(APIUrlDepartment,http);
  }

    //get with id
    getDepartmentBySite(id: any): Observable<any> {
      return this.httpC.get(`${APIUrlDepSite}/${id}`);
    }
}
