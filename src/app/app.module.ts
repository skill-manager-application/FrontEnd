import { HttpClientModule } from '@angular/common/http';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { SignInComponent } from './Components/sign-in/sign-in.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { authInterceptorProviders } from './interceptors/auth.interceptor';
import { DataService } from './Services/data.service';
import { UserService } from './Services/Soprahr/user.service';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { SpinnerComponent } from './Components/spinner/spinner.component';
import { HomeComponent } from './Components/dashboard/home/home.component';
import { ProfileComponent } from './Components/dashboard/profile/profile.component';
import { TeamsComponent } from './Components/dashboard/teams/teams.component';
import { UsersComponent } from './Components/dashboard/users/users.component';
import { AddEditUsersComponent } from './Components/dashboard/users/add-edit-users/add-edit-users.component';
import { SiteComponent } from './Components/dashboard/admin-interface/site/site.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { DepartmentInfoComponent } from './Components/dashboard/admin-interface/department-info/department-info.component';
import { RoleComponent } from './Components/dashboard/admin-interface/role/role.component';
import { QuestionsComponent } from './Components/dashboard/questions/questions.component';
import { ChangeBgDirective } from './Components/dashboard/questions/change-bg.directive';
import { SkillComponent } from './Components/dashboard/admin-interface/skill/skill.component';
import { SkillsComponent } from './Components/dashboard/profile/skills/skills.component';
import { OverviewComponent } from './Components/dashboard/profile/overview/overview.component';
import { SuperviserComponent } from './Components/dashboard/superviser/superviser.component';
import { AddEditSkillComponent } from './Components/dashboard/admin-interface/skill/add-edit-skill/add-edit-skill.component';
import { AssessmentComponent } from './Components/dashboard/assessment/assessment.component';
import { AssessComponent } from './Components/dashboard/superviser/assess/assess.component';
import { AssignComponent } from './Components/dashboard/superviser/assign/assign.component';
import { TeamDetailsComponent } from './Components/dashboard/teams/team-details/team-details.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ProjectDetailsComponent } from './Components/dashboard/Project/project-details/project-details.component';
import { ProjectComponent } from './Components/dashboard/Project/project.component';
import { TasksComponent } from './Components/dashboard/Project/tasks/tasks.component';
import { DetailTodoComponent } from './Components/dashboard/Project/tasks/detail-todo/detail-todo.component';
import { JwtModule } from "@auth0/angular-jwt";
import { ProfileAssessmentComponent } from './Components/dashboard/profile/profile-assessment/profile-assessment.component';
import { SkillOverViewComponent } from './Components/dashboard/skill-over-view/skill-over-view.component';

export function tokenGetter() {
  return localStorage.getItem("TOKEN_KEY");
}
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    DashboardComponent,
    SpinnerComponent,
    HomeComponent,
    ProfileComponent,
    TeamsComponent,
    UsersComponent,
    AddEditUsersComponent,
    SiteComponent,
    DepartmentInfoComponent,
    RoleComponent,
    QuestionsComponent,
    ChangeBgDirective,
    SkillComponent,
    SkillsComponent,
    OverviewComponent,
    SuperviserComponent,
    AddEditSkillComponent,
    AssessmentComponent,
    AssessComponent,
    AssignComponent,
    TeamDetailsComponent,
    ProjectDetailsComponent,
    ProjectComponent,
    TasksComponent,
    DetailTodoComponent,
    ProfileAssessmentComponent,
    SkillOverViewComponent,
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts'),
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:8080","localhost:8081","localhost:8082"],
        disallowedRoutes: [],
      },
    }),
  ],
  providers: [
    authInterceptorProviders,
    DataService,
    UserService,
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 3500}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
    ],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
