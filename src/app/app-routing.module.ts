import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './Components/sign-in/sign-in.component';
import { SecureInnerPagesGuard } from './GUARDS/secure-innerpages.guard';
import { AuthGuardGuard } from './GUARDS/auth-guard.guard';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { ProfileComponent } from './Components/dashboard/profile/profile.component';
import { UsersComponent } from './Components/dashboard/users/users.component';
import { HomeComponent } from './Components/dashboard/home/home.component';
import { NeedsComponent } from './Components/dashboard/needs/needs.component';
import { SiteComponent } from './Components/dashboard/admin-interface/site/site.component';
import { DepartmentInfoComponent } from './Components/dashboard/admin-interface/department-info/department-info.component';
import { RoleComponent } from './Components/dashboard/admin-interface/role/role.component';
import { QuestionsComponent } from './Components/dashboard/questions/questions.component';
import { SkillComponent } from './Components/dashboard/admin-interface/skill/skill.component';
import { SkillsComponent } from './Components/dashboard/profile/skills/skills.component';
import { OverviewComponent } from './Components/dashboard/profile/overview/overview.component';
import { SuperviserComponent } from './Components/dashboard/superviser/superviser.component';
import { AddEditSkillComponent } from './Components/dashboard/admin-interface/skill/add-edit-skill/add-edit-skill.component';
import { AssessmentComponent } from './Components/dashboard/assessment/assessment.component';
import { AssessComponent } from './Components/dashboard/superviser/assess/assess.component';
import { AssignComponent } from './Components/dashboard/superviser/assign/assign.component';
import { TeamsComponent } from './Components/dashboard/teams/teams.component';
import { TeamDetailsComponent } from './Components/dashboard/teams/team-details/team-details.component';
import { ProjectDetailsComponent } from './Components/dashboard/Project/project-details/project-details.component';
import { ProjectComponent } from './Components/dashboard/Project/project.component';
import { TasksComponent } from './Components/dashboard/Project/tasks/tasks.component';
import { DetailTodoComponent } from './Components/dashboard/Project/tasks/detail-todo/detail-todo.component';
import { HigherGrantedRoles,ManagerRole } from '../assets/roles.constants';
import { ProfileAssessmentComponent } from './Components/dashboard/profile/profile-assessment/profile-assessment.component';
import { SkillOverViewComponent } from './Components/dashboard/skill-over-view/skill-over-view.component';

/** @type {*} */
const routes: Routes = [
  {path: 'need', component: NeedsComponent },
  {path : "Quizz/:id" , component : QuestionsComponent, canActivate: [AuthGuardGuard] ,},
  {path : "SignIn" , component : SignInComponent, canActivate: [SecureInnerPagesGuard]},
  {
    path: 'Dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuardGuard] ,// this is the component with the <router-outlet> in the template
    children: [
      {
        path :"Users",
        component: UsersComponent,
        canActivate: [AuthGuardGuard],
        data: {
          role: HigherGrantedRoles
        }
      },
      {
        path :"Sites",
        component: SiteComponent,
        canActivate: [AuthGuardGuard]
      },
      {
        path :"Roles",
        component: RoleComponent,
        canActivate: [AuthGuardGuard],
        data: {
          role: HigherGrantedRoles
        }
      },
      {
        path : "Department/:id" ,
        component : DepartmentInfoComponent,
        canActivate: [AuthGuardGuard]
      },
      {
        path : "Skill" ,
        component : SkillOverViewComponent,
        canActivate: [AuthGuardGuard]
      },
      //Profile
      {
        path: 'Profile',
        component: ProfileComponent,
        canActivate: [AuthGuardGuard],
        children :[
          {path :"Skills",component: SkillsComponent},
          {path :"OverView",component: OverviewComponent},
          {path : "Assessment",component : ProfileAssessmentComponent},
          {
            path: '', redirectTo: 'OverView', pathMatch: 'full'
          },

        ]
      },
      {
        path: 'Profile/:id',
        component: ProfileComponent,
        canActivate: [AuthGuardGuard],
        children :[
          {path :"Skills",component: SkillsComponent},
          {path :"OverView",component: OverviewComponent},
          {path : "Assessment",component : ProfileAssessmentComponent},
          {
            path: '', redirectTo: 'OverView', pathMatch: 'full'
          },
        ]
      },
      // Project Routing
      {
        path: 'Project',
        component: ProjectComponent,
        canActivate: [AuthGuardGuard],

      },
      {
        path: 'Project/:id',
        component: ProjectDetailsComponent,
        canActivate: [AuthGuardGuard],
      },
      {
        path: 'Projects/Todos',
        component: TasksComponent,
        canActivate: [AuthGuardGuard],

      },
      {
        path: 'Project/:id/Todos',
        component: DetailTodoComponent,
        canActivate: [AuthGuardGuard],

      },
      {
        path: 'Project/:id/Todos/:id',
        component: DetailTodoComponent,
        canActivate: [AuthGuardGuard],

      },
      // Team Routing
      {
        path: 'Teams/Details/:id',
        component: TeamDetailsComponent,
        canActivate: [AuthGuardGuard]
      },
      {
        path: 'Teams',
        component: TeamsComponent,
        canActivate: [AuthGuardGuard]
      },
      // Assessment
      {
        path: 'Assessment/:id',
        component: AssessmentComponent,
        canActivate: [AuthGuardGuard]
      },
      {
        path: 'SuperViser',
        component: SuperviserComponent,
        canActivate: [AuthGuardGuard],
        data: {
          role: ManagerRole
        },
        children : [
          {
            path: '', redirectTo: 'Assess', pathMatch: 'full'
          },
          {path :"Assess",component: AssessComponent},
          {path :"Assign",component: AssignComponent},

        ]
      },
      {
        path: 'Home',
        component: HomeComponent,
        canActivate: [AuthGuardGuard]
      },
      // Skill Managemnt
      {path :"SkillsManagment",
      component: SkillComponent,
      canActivate: [AuthGuardGuard],
      data: {
        role: ManagerRole
      }
      },
      {path :"AddSkill",
      component: AddEditSkillComponent,
      canActivate: [AuthGuardGuard],
      data: {
        role: HigherGrantedRoles
      }
      },
      {path :"EditSkill/:id",
      component: AddEditSkillComponent,
      canActivate: [AuthGuardGuard],
      data: {
        role: HigherGrantedRoles
      }
      },
      {
        path: '', redirectTo: 'Home', pathMatch: 'full'
      },
    ],
  },
  { path: '',   redirectTo: '/Dashboard/Home', pathMatch: 'full' }, // redirect to

  { path: '**',  redirectTo: '/Dashboard/Home'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
// https://vsavkin.tumblr.com/post/146722301646/angular-router-empty-paths-componentless-routes
