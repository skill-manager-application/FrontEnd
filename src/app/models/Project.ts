import { Teams } from "./Teams";

export class Project {

  id : string;
  projectName : string;
  projectDesc : string;
  team : Teams;
  status :string;
  startDate : Date;
  endDate : Date;
  folder : Folder;
  superviserid: string;

  constructor() {
    this.id = "";
    this.projectName = "";
    this.projectDesc = "";
    this.team = new Teams();
    this.status = "";
    this.startDate = new Date();
    this.endDate = new Date();
    this.folder = new Folder(0,"",[]);
    this.superviserid = "";
  }
}

export class Folder{
  folderNum  : number;
  folderName  : string;
  todos  : any[];

  constructor(
    folderNum  : number,
    folderName  : string,
    todos  : any[]
  ) {
    this.folderNum = folderNum;
    this.folderName = folderName;
    this.todos = todos;
  }
}


