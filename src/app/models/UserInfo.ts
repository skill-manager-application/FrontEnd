import { Skill } from "./skill";

export class UserInfo {
  id ?: string;

  userId ?: string;

  skillLevel ?: number;

  interstLevel ?: number;

  competencyLevel ?: number;

  superViserId ?: string;
  skills : Skill[] = [];

}
export class Skills {
  skillId ?: string;
  skillName ?: string;
  selfAssignment ?: number;
  supervisorAssignment ?: number;
  intersetLevel ?: number;
  avgLevel ?: number;
  scroce ?: number;
  testToken ?: boolean;
  passedAt ?: Date;
  assigneddAt ?: Date;
  assessmentToken ?: Date;

}
