export class Task{
  id : string ;
  desc: string ;
  supervisorId: string ;
  progress: number ;
userIds: string[] ;
  priorty: string ;

  startTime: Date ;
  endTime: string ;
  name: string ;
  status: string ;
  folderId: number ;
  projectId: string ;
  checkList: checkList[] ;
  constructor() {
    this.id = '';
    this.desc = '';
    this.supervisorId = '';
    this.progress = 0;
    this.userIds = [];
    this.priorty = '';
    this.startTime = new Date();
    this.endTime = "";
    this.name = '';
    this.status = '';
    this.folderId = 0;
    this.projectId = '';
    this.checkList = [];
  }
}
export class checkList{
   text :string;
		  check :boolean;
      updated : Date  = new Date();
      userId : string ='';
      constructor(){
        this.text = '';
        this.check = false;
        this.updated = new Date();
        this.userId ='';
      }
}
