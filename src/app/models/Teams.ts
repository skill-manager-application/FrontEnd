export class Teams {
  id : string | null;
  name : string;
  usersId : string[];
  status : string;
  projects : any[];
  departmentId : string;
  constructor() {
    this.id = null;
    this.name = "";
    this.usersId = [];
    this.status = "";
    this.projects = [];
    this.departmentId = "";
  }
}
