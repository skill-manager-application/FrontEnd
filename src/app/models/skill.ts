export class Skill {

  id ?: string;
  name : string;
  description :string;
  level : number;
  keywords : string[];
  categoryID : string;
  categoryName : string;

  constructor(){
    this.name = "";
    this.description = "";
    this.level = 0;
    this.keywords = [];
    this.categoryID = "";
    this.categoryName = "";
  }
}
