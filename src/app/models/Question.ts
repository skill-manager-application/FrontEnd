export class Question {
  id ?: string;
  questionText ?: string;
  explanation ?:string;
  options ?: Options[];
  skillId ?: string;
}
export class Options {
  text ?: string;
  correct ?:boolean;
}

