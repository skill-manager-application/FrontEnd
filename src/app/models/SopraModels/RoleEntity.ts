export class RoleEntity {
  idRole : number;
  roleName : string;
  job_title : string;

  public constructor(
    idRole: number,
    roleName: string,
    job_title: string) {
      this.idRole = idRole;
      this.roleName = roleName;
      this.job_title = job_title;
    }
}
