import { RoleEntity } from "./RoleEntity";

export class UserEntity {
  [x: string]: any;
  userId : string;
  firstname : string;
  lastname : string;
  email : string;

  siteId : number;
  siteName : string;

  departmentId : string;
  departmentDomain : string;

  roleId : string;
  role : RoleEntity ;

  public constructor(
    userId: string,
    firstname: string,
    lastname: string,
    email: string,
    siteId: number,
    siteName: string,
    departmentId: string,
    departmentDomain: string,
    roleId: string,
    role : RoleEntity
  ) {
    this.userId = userId;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.siteId = siteId;
    this.siteName = siteName;
    this.departmentId = departmentId;
    this.departmentDomain = departmentDomain;
    this.roleId = roleId;
    this.role = role;
  }
}
