import { DepartmentEntity } from './DepartmentEntity';

export class SiteEntity {
  id : number;
  name : string;
  local : string;
  country : string;
  departments : DepartmentEntity[]=[];
  public constructor(
    id: number,
    name: string,
    local: string,
    country: string) {
      this.id = id;
      this.name = name;
      this.local = local;
      this.country = country;
      }
}
