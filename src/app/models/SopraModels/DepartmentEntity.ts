export class DepartmentEntity {
  id : number;
  domain : string;
  departmentMotherId : number;
  departmentType : string;
  siteId : number;

  public constructor(
    id: number,
    domain: string,
    departmentMotherId: number,
    departmentType: string,
    siteId: number,
    ){
      this.id = id;
      this.domain = domain;
      this.departmentMotherId = departmentMotherId;
      this.departmentType = departmentType;
      this.siteId = siteId;
    }
}
